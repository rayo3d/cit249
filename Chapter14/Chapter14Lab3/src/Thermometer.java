import javax.swing.JApplet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.BorderFactory;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 3-26-2016
 * @purpose This program demonstrates a solution to the Thermometer Applet programming challenge.
 */

public class Thermometer extends JApplet {
	// Declare constants and objects needed by Thermometer class
	private final int MIN_TEMP = 0;
	private final int MAX_TEMP = 250;
	private final int INITIAL_TEMP = 0;
	private final int TICK_SPACING = 50;
	
	private ThermoPanel thermoPanel;
	private JPanel sliderPanel;
	private JSlider slider;
	
	// Initialize method
	public void init() {
		// Create the ThermoPanel object.
		thermoPanel = new ThermoPanel();
		
		// Build the slider panel.
		buildSliderPanel();
		
		// Add the ThermoPanel to the center region of the content pane.
		add(thermoPanel, BorderLayout.CENTER);
		
		// Add the slider panel to the south region of the content pane.
		add(sliderPanel, BorderLayout.SOUTH);
	}
	
	/*
	* The buildSliderPanel method builds a panel for the slider.
	*/
	private void buildSliderPanel() {
		// Create the slider panel.
		sliderPanel = new JPanel();
		
		// Create and configure the slider.
		slider = new JSlider(JSlider.HORIZONTAL, MIN_TEMP, MAX_TEMP, INITIAL_TEMP);
		slider.setMajorTickSpacing(TICK_SPACING);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.addChangeListener(new SliderListener());
		
		// Add the slider to the panel.
		sliderPanel.add(slider);
		
		// Put a line border around the panel.
		sliderPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare objects needed by main method
		Thermometer applet = new Thermometer();
		JFrame frame = new JFrame();
		
		frame.add(applet, BorderLayout.CENTER);
		
		applet.init();
		applet.start();
		
		frame.setTitle("Change Shape Color Example Using JSliders");
		frame.setVisible(true);
		frame.setSize(300, 430);
	}

	/*
	* Inner class to handle the slider's change events
	*/
	private class SliderListener implements ChangeListener {
		/*
		* stateChanged method
		* @param e A ChangeEvent object.
		*/
		public void stateChanged(ChangeEvent e) {
			int t = slider.getValue();
			thermoPanel.setTemperature(t);
		}
	}
}
