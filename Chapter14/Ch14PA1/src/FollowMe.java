import javax.swing.JApplet;

import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 3-27-2016
 * @purpose This program demonstrates a solution to the FollowMe Applet programming challenge
 */

public class FollowMe extends JApplet {
	// Constants for the label state.
	private final int TEXT_CENTER = 0; // Label Center
	private final int TEXT_UP = 1; // Label Up
	private final int TEXT_DOWN = 2; // Label Down
	private final int TEXT_LEFT = 3; // Label Left
	private final int TEXT_RIGHT = 4; // Label Right
	
	// Setup text
	private final String TEXT = "Hello";
	private static int fontSize = 16;
	private static Font font = new Font("Helvetica", Font.PLAIN, fontSize);
	
	// Label center position
	private final int TEXT_CENTER_X = 100, TEXT_CENTER_Y = 100;
	
	// Label down position
	private final int TEXT_DOWN_X = 100, TEXT_DOWN_Y = 175;
	
	// Label up position
	private final int TEXT_UP_X = 100, TEXT_UP_Y = 25;
	
	// Label right position
	private final int TEXT_RIGHT_X = 175, TEXT_RIGHT_Y = 100;
	
	// Label left position
	private final int TEXT_LEFT_X = 25, TEXT_LEFT_Y = 100;
	
	private int textState = TEXT_CENTER; // Current state of the label
	
	// Initialize method
	public void init() {
		// Set the background color to white.
		getContentPane().setBackground(Color.white);
		
		// Add the mouse motion listener.
		addMouseMotionListener(new MyMouseMotionListener());
	}
	
	/*
	* The paint method draws the label in the correct position.
	* @param g The applet's Graphics object.
	*/
	public void paint(Graphics g) {
		// Call the base class paint method.
		super.paint(g);
		
		g.setFont(font);
		
		// Draw the pupils in the correct position.
		if (textState == TEXT_CENTER)
			drawTextCentered(g);
		else if (textState == TEXT_UP)
			drawTextUp(g);
		else if (textState == TEXT_DOWN)
			drawTextDown(g);
		else if (textState == TEXT_LEFT)
			drawTextLeft(g);
		else if (textState == TEXT_RIGHT)
			drawTextRight(g);
		else
			drawTextCentered(g);
	}
	
	/*
	* The drawTextAhead method draws the label centered.
	*/
	public void drawTextCentered(Graphics g) {
		g.drawString(TEXT, TEXT_CENTER_X, TEXT_CENTER_Y);
	}
	
	/*
	* The drawTextDown method sets the label position to down.
	*/
	public void drawTextDown(Graphics g) {
		g.drawString(TEXT, TEXT_DOWN_X, TEXT_DOWN_Y);
	}
	
	/*
	* The drawTextUp method sets the label position to up.
	*/
	public void drawTextUp(Graphics g) {
		g.drawString(TEXT, TEXT_UP_X, TEXT_UP_Y);
	}
	
	/*
	* The drawTextRight method sets the label position to right.
	*/
	public void drawTextRight(Graphics g) {
		g.drawString(TEXT, TEXT_RIGHT_X, TEXT_RIGHT_Y);
	}
	
	/*
	* The drawTextLeft method sets the label position to left.
	* @param g The applet's Graphics object.
	*/
	public void drawTextLeft(Graphics g) {
		g.drawString(TEXT, TEXT_LEFT_X, TEXT_LEFT_Y);
	}
	
	/*
	* MyMouseMotionListener private inner class
	*/
	private class MyMouseMotionListener extends MouseMotionAdapter {
		/*
		* mouseMoved method
		* @param e The MouseEvent object for this event.
		*/
		public void mouseMoved(MouseEvent e) {
			// Get the mouse pointer's X and Y coordinates.
			int x = e.getX(), y = e.getY();
			
			// Determine the correct eye state.
			if (y < TEXT_CENTER_Y)
				textState = TEXT_UP;
			else if (y > (TEXT_CENTER_Y + fontSize))
				textState = TEXT_DOWN;
			else if (x < TEXT_CENTER_X)
				textState = TEXT_LEFT;
			else if (x > (TEXT_CENTER_X + fontSize))
				textState = TEXT_RIGHT;
			
			// Force a repaint.
			repaint();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare objects needed by main method
		FollowMe applet = new FollowMe();
		JFrame frame = new JFrame();
		
		frame.add(applet, BorderLayout.CENTER);
		
		applet.init();
		applet.start();
		
		frame.setTitle("Change the position of text based on a mouse's movements");
		frame.setVisible(true);
		frame.setSize(300, 300);
	}

}
