import java.util.ArrayList;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-28-2016
 * @purpose Read/write vending machine inventory
 */

public class Vending {
	// Declare items needed for Vending class
	private static ArrayList <VendingInfo> vendingInfo = new ArrayList<VendingInfo>();
	private static File vendingFile = new File("MachineInventory.dat");
	
	// Read MachineInventory.dat file and assign values to class variables
	public static ArrayList <VendingInfo> readVendingInventory() {
		try {
			FileReader fr = new FileReader(vendingFile);
			BufferedReader in = new BufferedReader(fr);
			String drinkDescription;
			int drinkInventory;
			
			while(true) {
				drinkDescription = in.readLine();
				
				if(drinkDescription == null) break;
				
				drinkInventory = Integer.parseInt(in.readLine());
				
				// Create vending info instance and add reference to ArrayList vendingInfo
				if(drinkDescription != null) {
					vendingInfo.add(new VendingInfo(drinkDescription, drinkInventory));
				}
			}//end while
		}//end try
		
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading file");
		}
		
		return vendingInfo;
	}
}
