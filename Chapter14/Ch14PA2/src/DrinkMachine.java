import javax.swing.JApplet;

import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.BorderLayout;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;

import java.util.ArrayList;

import java.text.NumberFormat;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-28-16
 * @purpose Simulate a vending machine by allowing a user to enter the money inserted into the machine, choosing a drink, then getting a message about how many drinks are left and report their change
 */

public class DrinkMachine extends JApplet {
	// Declare objects needed by DrinkMachine class
	private ArrayList <VendingInfo> drinkList;
	private VendingInfo aDrink;
	private JLabel drinkLabel, tenderLabel;
	private JButton[] buttons = new JButton[5];
	private Font largeFont, regularFont;
	private JPanel labelPanel, buttonPanel, tenderPanel;
	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	private JTextField tenderField;
	private final double DRINK_COST = 0.75;
	
	// Initialize method
	public void init() {
		// Initialize Vending Info
		drinkList = Vending.readVendingInventory();
		
		// Build Label Panel
		buildLabelPanel();
		
		// Build Button Panel
		buildButtonPanel();
		
		// Build Tender Panel
		buildTenderPanel();
		
		// Add the Label Panel to the left region of the content pane.
		add(labelPanel, BorderLayout.WEST);
		
		// Add the Button Panel to the right region of the content pane.
		add(buttonPanel, BorderLayout.EAST);
		
		// Add the Tender panel to the bottom region of the content pane.
		add(tenderPanel, BorderLayout.SOUTH);
	}
	
	// Create Label Panel
	public void buildLabelPanel() {
		labelPanel = new JPanel(new GridLayout(1,1));
		
		largeFont = new Font("Helvetica", Font.BOLD,24);
		
		drinkLabel = new JLabel("Soft Drinks .75 Each");
		drinkLabel.setFont(largeFont);
		
		labelPanel.add(drinkLabel);
	}
	
	// Construct buttons and add listeners to each button
	public void buildButtonPanel() {
		buttonPanel = new JPanel(new GridLayout(5,1));
		Dimension size = new Dimension(175,20);
		regularFont = new Font("Helvetica", Font.BOLD, 14);
		
		ActionListener listener = new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            if (e.getSource() instanceof JButton) {
	                int clickedButton = (int) ((JButton) e.getSource()).getClientProperty("button");
	                
	                aDrink = (VendingInfo) drinkList.get(clickedButton);
	                
					if (aDrink.getDrinkInventory() > 0)
						purchaseDrink(aDrink);
					else
						JOptionPane.showMessageDialog(buttonPanel,"Sorry, " + aDrink.getDrinkDescription() + " is sold out.");
	            }
	        }
	    };
		
		for(int i = 0; i < buttons.length; i++) {
			aDrink = (VendingInfo) drinkList.get(i);
			
			buttons[i] = new JButton(aDrink.getDrinkDescription());
			buttons[i].setPreferredSize(size);
			buttons[i].setFont(regularFont);
			buttons[i].putClientProperty("button", i);
			buttons[i].addActionListener(listener);
			
			buttonPanel.add(buttons[i]);
		}
	}
	
	// Create Tender Panel
	public void buildTenderPanel() {
		tenderPanel = new JPanel(new GridLayout(1,2));
		tenderLabel = new JLabel("Enter Amount Tendered: ", SwingConstants.CENTER);
		tenderField = new JTextField("0.00");
		
		tenderField.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
            	tenderField.setText("");
            }
        });
		
		tenderPanel.add(tenderLabel);
		tenderPanel.add(tenderField);
	}
	
	// Process purchasing drink
	public void purchaseDrink(VendingInfo boughtDrink) {
		double changeAmount = 0.0;
		
		try {
			if((tenderField.toString()).length() == 0)
				JOptionPane.showMessageDialog(this, "Please enter a tender amount.");
			else {
				double tenderAmount = Double.parseDouble(tenderField.getText());
				
				if (tenderAmount < DRINK_COST)
					JOptionPane.showMessageDialog(this,"You must enter more money to purchase a drink.");
				else {
					changeAmount = tenderAmount - DRINK_COST;
					
					JOptionPane.showMessageDialog(this,"1 drink has been dispensed.\n" + currencyFormat.format(changeAmount) + " in change given back.");
					
					tenderField.setText("0.00");
					boughtDrink.setDrinkInventory((boughtDrink.getDrinkInventory() - 1));
				}
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this,"Tender amount must be a number.");
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare objects needed by main method
		DrinkMachine applet = new DrinkMachine();
		JFrame frame = new JFrame();
		
		frame.add(applet, BorderLayout.CENTER);
		
		applet.init();
		applet.start();
		
		frame.setTitle("Simulate a Vending machine");
		frame.setVisible(true);
		frame.setSize(450, 500);
	}

}
