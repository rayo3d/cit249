/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-28-2016
 * @purpose 
 */

public class VendingInfo {
	// Attributes
	private String drinkDescription;
	private int drinkInventory;
	
	// Constructor
	public VendingInfo(String aDrinkDescription, int aDrinkInventory) {
		setDrinkDescription(aDrinkDescription);
		setDrinkInventory(aDrinkInventory);
	}
	
	// Set mutator methods with simple validation
	public void setDrinkDescription(String aDrinkDescription) {
		drinkDescription = aDrinkDescription;
	}
	
	public void setDrinkInventory(int aDrinkInventory) {
		drinkInventory = aDrinkInventory;
	}
	
	// Get accessor methods
	public String getDrinkDescription() {
		return drinkDescription;
	}
	
	public int getDrinkInventory() {
		return drinkInventory;
	}
}
