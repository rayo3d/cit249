import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-26-2016
 * @purpose Demo drawing a star and changing its position using on screen buttons
 */

public class Ch14Lab1 extends JApplet {
	// Declare objects needed for Ch14Lab1 class
	private Star star;
	private JPanel buttonPanel;
	private JButton leftButton, rightButton, upButton, downButton;
	
	// Initialize objects
	public void init() {
		star = new Star();
		leftButton = new JButton("Left");
		rightButton = new JButton("Right");
		upButton = new JButton("Up");
		downButton = new JButton("Down");
		
		// Add listeners to buttons
		leftButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				star.setX(-10);
			}
		});
		
		rightButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				star.setX(10);
			}
		});
		
		upButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				star.setY(-10);
			}
		});
		
		downButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				star.setY(10);
			}
		});
		
		buttonPanel = new JPanel();
		
		buttonPanel.add(leftButton);
		buttonPanel.add(rightButton);
		buttonPanel.add(upButton);
		buttonPanel.add(downButton);
		
		add(star, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare objects needed by Main method
		Ch14Lab1 applet = new Ch14Lab1();
		JFrame frame = new JFrame();
		
		frame.add(applet, BorderLayout.CENTER);
		
		applet.init();
		applet.start();
		applet.setSize(500,500);
		
		frame.setTitle("Shape Examples");
		frame.setVisible(true);
		frame.setSize(500,500);
	}

}
