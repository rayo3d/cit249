import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.GeneralPath;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-26-2016
 * @purpose Construct the shape of a star, set its color and X and Y coordinates. A GeneralPath object is needed to draw the path of the shape.
 */

public class Star extends JPanel {
	// Declare objects needed for Star class
	private GeneralPath star;
	private int x, y;
	private Graphics2D g1;
	private double points[][] = {
		{ 0, 85 }, { 75, 75 }, { 100, 10 }, { 125, 75 },
		{ 200, 85 }, { 150, 125 }, { 160, 190 }, { 100, 150 },
		{ 40, 190 }, { 50, 125 }, { 0, 85 }
	};
	
	// Constructor
	public Star() {
		star = new GeneralPath();
		x = 125;
		y = 75;
	}
	
	// Pass Graphics object to another method to draw the star
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		doDrawing(g);
	}
	
	// Draw the star
	private void doDrawing(Graphics g) {
		g1 = (Graphics2D)g;
		g1.setColor(Color.BLUE);
		
		g1.translate(x, y);
		
		star.moveTo(points[0][0], points[0][1]);
		
		for (int k = 1; k < points.length; k++)
			star.lineTo(points[k][0], points[k][1]);
		
		star.closePath();
		
		repaint();
		
		g1.fill(star);
	}
	
	// Mutator methods
	public void setX(int n) {
		x = x + n;
		repaint();
	}
	public void setY(int n) {
		y = y + n;
		repaint();
	}
}
