import javax.swing.JApplet;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.BorderFactory;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 3-26-2016
 * @purpose Change the color of an object using JSliders. One value of three sliders will determine the RGB color.
 */

public class Ch14Lab2Square extends JApplet implements ChangeListener {
	// Declare objects needed by Ch14Lab2Square class
	private Square square;
	private JSlider color1Slider, color2Slider, color3Slider;
	private JPanel sliderPanel, buttonPanel, bottomPanel;
	private JButton leftButton, rightButton;
	private Color squareColor;
	private Border emptyBdr = BorderFactory.createEmptyBorder(20,20,20,20);
	
	// Initialize method
	public void init() {
		square = new Square();
		
		color1Slider = new JSlider(JSlider.HORIZONTAL, 0, 255, 85);
		color1Slider.setMajorTickSpacing(15);
		color1Slider.setMinorTickSpacing(5);
		color1Slider.setPaintTicks(true);
		color1Slider.setPaintLabels(true);
		color1Slider.addChangeListener(this);
		
		color2Slider = new JSlider(JSlider.HORIZONTAL, 0, 255, 125);
		color2Slider.setMajorTickSpacing(15);
		color2Slider.setMinorTickSpacing(5);
		color2Slider.setPaintTicks(true);
		color2Slider.setPaintLabels(true);
		color2Slider.addChangeListener(this);
		
		color3Slider = new JSlider(JSlider.HORIZONTAL, 0, 255, 95);
		color3Slider.setMajorTickSpacing(15);
		color3Slider.setMinorTickSpacing(5);
		color3Slider.setPaintTicks(true);
		color3Slider.setPaintLabels(true);
		color3Slider.addChangeListener(this);
		
		leftButton = new JButton("<==");
		rightButton = new JButton("==>");
		
		leftButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				square.setX(-10);
			}
		});
		
		rightButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				square.setX(10);
			}
		});
		
		bottomPanel = new JPanel(new GridLayout(2,1));
		sliderPanel = new JPanel();
		sliderPanel.setLayout(new GridLayout(3,1));
		sliderPanel.setBorder(emptyBdr);
		buttonPanel = new JPanel();
		sliderPanel.add(color1Slider);
		sliderPanel.add(color2Slider);
		sliderPanel.add(color3Slider);
		buttonPanel.add(leftButton);
		buttonPanel.add(rightButton);
		
		bottomPanel.add(sliderPanel);
		bottomPanel.add(buttonPanel);
		
		squareColor = new Color(color1Slider.getValue(), color2Slider.getValue(), color3Slider.getValue());
		
		add(sliderPanel, BorderLayout.NORTH);
		add(square, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent arg0) {
		// Declare objects needed by stateChanged class
		squareColor = new Color(color1Slider.getValue(), color2Slider.getValue(), color3Slider.getValue());
		
		repaint();
	}
	
	// Create Square inner class
	class Square extends JPanel {
		// Declare objects needed by Square class
		int x = 250, y = 0;
		
		// Constructor
		public Square() {
		}
		
		// Create rectangle
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			g.setColor(squareColor);
			g.fillRect(x, y, 300, 300);
		}
		
		// Mutator
		public void setX(int n) {
			x = x + n;
			
			repaint();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare objects needed by main method
		Ch14Lab2Square applet = new Ch14Lab2Square();
		JFrame frame = new JFrame();
		
		frame.add(applet, BorderLayout.CENTER);
		
		applet.init();
		applet.start();
		
		frame.setTitle("Change Shape Color Example Using JSliders");
		frame.setVisible(true);
		frame.setSize(800,600);
	}

}
