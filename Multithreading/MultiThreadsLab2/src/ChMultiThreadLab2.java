import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JButton;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 4-26-16
 * @purpose This demonstrates the solution to the the RandomNumbers multithreading programming challenge
 */

public class ChMultiThreadLab2 extends JFrame {
	//Declare two number panels
	private NumberControl control1, control2;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create frame
		ChMultiThreadLab2 frame = new ChMultiThreadLab2();
		
		// Display the frame
		frame.setSize(600, 400);
		frame.setTitle("MultiThreads Lab 2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/** Initialize the frame */
	public ChMultiThreadLab2() {
		// Panel p1 for holding two numbers
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 2));
		
		// Create a number to display on the left
		p1.add(control1 = new NumberControl());
		
		// Create a number to display on the right
		p1.add(control2 = new NumberControl());
		
		// Add p1 to the frame
		add(p1, BorderLayout.CENTER);
	}
	
	// NumberControl inner class
	class NumberControl extends JPanel implements ActionListener {
		private Numbers numbers = new Numbers();
		private JButton jbtSuspend = new JButton("Suspend");
		private JButton jbtResume = new JButton("Resume");
		
		// Constructor
		public NumberControl() {
			// Group buttons in a panel
			JPanel panel = new JPanel();
			panel.add(jbtSuspend);
			panel.add(jbtResume);
			
			// Add number and buttons to the panel
			setLayout(new BorderLayout());
			add(numbers, BorderLayout.CENTER);
			add(panel, BorderLayout.SOUTH);
			
			// Register Listeners
			jbtSuspend.addActionListener(this);
			jbtResume.addActionListener(this);
		}
		
		// Listener for action events
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == jbtSuspend) {
				numbers.suspend();
			} else if (e.getSource() == jbtResume) {
				numbers.resume();
			}
		}
		
		// Numbers inner class
		class Numbers extends RandomNumbers implements Runnable {
			private boolean suspended;
			
			// Constructor
			public Numbers() {
				new Thread(this).start();
			}
			
			// Run method
			public void run() {
				while (true) {
					draw();
					repaint();
					
					try {
						Thread.sleep(1000);
						waitForNotificationToResume();
					}
					catch (InterruptedException ex) {
						
					}
				}
			}
			
			// Suspend the thread
			public synchronized void suspend() {
				suspended = true;
			}
			
			// Resume the thread
			public synchronized void resume() {
				suspended = false;
				notifyAll();
			}
			
			// Set waiting to resume
			private synchronized void waitForNotificationToResume() throws InterruptedException {
				while (suspended) {
					wait();
				}
			}
		}
	}
}
