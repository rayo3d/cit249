import javax.swing.JPanel;

import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 4-26-16
 * @purpose This is the the RandomNumbers class
 */

public class RandomNumbers extends JPanel {
	// Declare objects needed for RandomNumbers class
	int maximum = 500;
	int minimum = 0;
	
	Random rn = new Random();
	int range = maximum - minimum + 1;
	int randomNum = rn.nextInt(range) + minimum;
	
	final int XPOSITION = 80;
	final int SIZE = 50;
	final int YPOSITION = 200;
	
	// Paint the numbers on screen
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setFont(new Font("TimesRoman", Font.PLAIN, 72));
		
		// Draw the number
		g.drawString(String.valueOf(randomNum), XPOSITION, YPOSITION);
	}
	
	// Allow redraw of a new number on screen
	public void draw() {
		randomNum = rn.nextInt(range) + minimum;
	}
}
