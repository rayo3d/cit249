import javax.swing.JFrame;

import javax.swing.JPanel;
import java.awt.Image;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import java.awt.Graphics2D;
import java.awt.Color;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 4-26-16
 * @purpose This program demonstrates the solution to the BallThread multithreading programming challenge
 */

public class BallThread extends JFrame {
	// Declare objects needed for for BallThread class
	MoveItBall b = new MoveItBall();
	
	// Constructor
	BallThread() {
		add(b);
		setTitle("Bouncing Ball");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(550, 400);
		setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create instance of the class
		new BallThread();
	}

	// Create MoveItBall inner class
	class MoveItBall extends JPanel {
		final int XPOSITION = 225;
		final int SIZE = 50;
		int yposition = 0;
		int yincrements = 2, yPositionTop = 0, yPositionBottom = 300;
		MoveBallThread b = new MoveBallThread();
		
		// Constructor
		MoveItBall() {
			Thread thread = new Thread(b);
			thread.start();
		} // end constructor
		
		// Create ball graphic
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D)g;
			setBackground(Color.CYAN);
			
			g2d.fillOval(XPOSITION, yposition, SIZE, SIZE);
		}
		
		// Create MoveBallThread inner class
		public class MoveBallThread extends Thread {
			// Movement of the ball
			public void move() {
				if (yposition > yPositionBottom) {
					yincrements = -10;
				} else if (yposition <= yPositionTop) {
					yincrements = 10;
				}
				
				yposition += yincrements;
			}
			
			// Run Thread to play animation
			public void run() {
				while (true) {
					move();
					
					try {
						Thread.sleep(20);
					}
					catch(Exception e) {
					}
					
					repaint();
				}
			}
		}
	}
}
