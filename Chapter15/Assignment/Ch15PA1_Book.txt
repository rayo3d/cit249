Name Formatter

Create a JavaFX application that lets the user enter the following pieces of data:

    The user�s first name

    The user�s middle name

    The user�s last name

    The user�s preferred title (Mr., Mrs., Ms., Dr., etc.)

Assume the user has entered the following data:

    First name: Kelly

    Middle name: Jane

    Last name: Smith

    Title: Ms.

The application should have buttons that display the user�s name formatted in the following ways:

    Ms. Kelly Jane Smith

    Kelly Jane Smith

    Kelly Smith

    Smith, Kelly Jane, Ms.

    Smith, Kelly Jane

    Smith, Kelly