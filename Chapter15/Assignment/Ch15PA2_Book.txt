Rock, Paper, Scissors Game

Create a JavaFX application that lets the user play the game of rock, paper, scissors against the computer. The program should work as follows.

    When the program begins, a random number in the range of 1 through 3 is generated. If the number is 1, then the computer has chosen rock. If the number is 2, then the computer has chosen paper. If the number is 3, then the computer has chosen scissors. (Do not display the computer�s choice yet.)

    The user selects his or her choice of rock, paper, or scissors by clicking a Button. An image of the user�s choice should be displayed in an ImageView component. (You will find rock, paper, and scissors image files in the book�s Student Sample Files.)

    An image of the computer�s choice is displayed.

    A winner is selected according to the following rules:

        If one player chooses rock and the other player chooses scissors, then rock wins. (Rock smashes scissors.)

        If one player chooses scissors and the other player chooses paper, then scissors wins. (Scissors cut paper.)

        If one player chooses paper and the other player chooses rock, then paper wins. (Paper wraps rock.)

        If both players make the same choice, the game must be played again to determine the winner.