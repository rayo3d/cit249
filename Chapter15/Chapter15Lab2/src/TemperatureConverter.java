import javafx.application.Application;
import javafx.stage.Stage;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 4-5-2016
 * @purpose This program demonstrates a solution to the Temperature Converter Application programming challenge
 */

public class TemperatureConverter extends Application {

	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage arg0) throws Exception {
		// Load the FXML file
		Parent parent = FXMLLoader.load(getClass().getResource("TemperatureConverter.fxml"));
		
		// Build the scene graph
		Scene scene = new Scene(parent);
		
		// Display our window, using the scene graph
		arg0.setTitle("Temperature Converter");
		arg0.setScene(scene);
		arg0.show();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Launch the application
		launch(args);
	}

}
