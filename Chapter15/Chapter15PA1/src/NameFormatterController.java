import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 4-12-2016
 * @purpose This is the controller class for the Name Formatter programming challenge
 */

public class NameFormatterController {
	@FXML
	private TextField firstNameTextField;
	
	@FXML
	private TextField middleNameTextField;
	
	@FXML
	private TextField lastNameTextField;
	
	@FXML
	private TextField preferredTitleTextField;
	
	@FXML
	private Label firstNameLabel;
	
	@FXML
	private Label middleNameLabel;
	
	@FXML
	private Label lastNameLabel;
	
	@FXML
	private Label preferredTitleLabel;
	
	@FXML
	private Label outputLabel;
	
	@FXML
	private Button format1Button;
	
	@FXML
	private Button format2Button;
	
	@FXML
	private Button format3Button;
	
	@FXML
	private Button format4Button;
	
	@FXML
	private Button format5Button;
	
	@FXML
	private Button format6Button;
	
	// Set variables needed for this class
	String firstName = "", middleName = "", lastName = "", preferredTitle = "";
	
	public void initialize() {
		outputLabel.setAlignment(Pos.CENTER);
	}
	
	// Event listener for the format1ButtonListener
	public void format1ButtonListener() {
		// Get the input from the TextFields
		firstName = firstNameTextField.getText();
		middleName = middleNameTextField.getText();
		lastName = lastNameTextField.getText();
		preferredTitle = preferredTitleTextField.getText();
		
		// Display the final format in the outputLabel
		outputLabel.setText(preferredTitle + " " + firstName + " " + middleName + " " + lastName);
	}
	
	// Event listener for the format2ButtonListener
	public void format2ButtonListener() {
		// Get the input from the TextFields
		firstName = firstNameTextField.getText();
		middleName = middleNameTextField.getText();
		lastName = lastNameTextField.getText();
		
		// Display the final format in the outputLabel
		outputLabel.setText(firstName + " " + middleName + " " + lastName);
	}
	
	// Event listener for the format3ButtonListener
	public void format3ButtonListener() {
		// Get the input from the TextFields
		firstName = firstNameTextField.getText();
		lastName = lastNameTextField.getText();
		
		// Display the final format in the outputLabel
		outputLabel.setText(firstName + " " + lastName);
	}
	
	// Event listener for the format4ButtonListener
	public void format4ButtonListener() {
		// Get the input from the TextFields
		firstName = firstNameTextField.getText();
		middleName = middleNameTextField.getText();
		lastName = lastNameTextField.getText();
		preferredTitle = preferredTitleTextField.getText();
		
		// Display the final format in the outputLabel
		outputLabel.setText(lastName + ", " + firstName + " " + middleName + ", " + preferredTitle);
	}
	
	// Event listener for the format5ButtonListener
	public void format5ButtonListener() {
		// Get the input from the TextFields
		firstName = firstNameTextField.getText();
		middleName = middleNameTextField.getText();
		lastName = lastNameTextField.getText();
		
		// Display the final format in the outputLabel
		outputLabel.setText(lastName + ", " + firstName + " " + middleName);
	}
	
	// Event listener for the format6ButtonListener
	public void format6ButtonListener() {
		// Get the input from the TextFields
		firstName = firstNameTextField.getText();
		lastName = lastNameTextField.getText();
		
		// Display the final format in the outputLabel
		outputLabel.setText(lastName + ", " + firstName);
	}
}
