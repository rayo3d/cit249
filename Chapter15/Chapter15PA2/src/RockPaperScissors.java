import javafx.application.Application;
import javafx.stage.Stage;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 4-13-2016
 * @purpose This program demonstrates a solution to the Rock, Paper, Scissors programming challenge
 */

public class RockPaperScissors extends Application {

	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage arg0) throws Exception {
		// Load the FXML file
		Parent parent = FXMLLoader.load(getClass().getResource("RockPaperScissors.fxml"));
		
		// Build the scene graph
		Scene scene = new Scene(parent);
		
		// Display our window, using the scene graph
		arg0.setTitle("Rock, Paper, Scissors");
		arg0.setScene(scene);
		arg0.show();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Launch the application
		launch(args);
	}

}
