import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.util.Random; // Needed for Random class

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 4-13-2016
 * @purpose This is the controller class for the Rock, Paper, Scissors programming challenge
 */

public class RockPaperScissorsController {
	@FXML
	private Button newGameButton;
	
	@FXML
	private Button rockButton;
	
	@FXML
	private Button paperButton;
	
	@FXML
	private Button scissorsButton;
	
	@FXML
	private ImageView playerImageView;
	
	@FXML
	private ImageView cpuImageView;
	
	@FXML
	private Label outputLabel;
	
	@FXML
	private Label playerResultLabel;
	
	@FXML
	private Label cpuResultLabel;
	
	// Named constants
	private final int SIZE = 3; // Number of Image objects
	
	// Private fields
	private Random rand; // A random number
	private Image rockImage; // The rock image
	private Image paperImage; // The paper image
	private Image scissorsImage; // The scissors image
	private ImageView playerImage, cpuImage; // To hold the ImageView components
	private Image[] images; // To hold the image objects
	private int cpuChoice = -1; // CPU random choice int
	private int playerChoice = -1; // Player choice int
	private boolean canPlay = false; // Ready to play check
	
	// This method is called when the FXML file is loaded
	public void initialize() {
		// Create a Random object
		rand = new Random();
		
		// Load the images
		rockImage = new Image("file:images/Rock.png");
		paperImage = new Image("file:images/Paper.png");
		scissorsImage = new Image("file:images/Scissors.png");
		
		// Create an array of Image objects
		images = new Image[SIZE];
		
		// Store the Image objects in the images array
		images[0] = rockImage;
		images[1] = paperImage;
		images[2] = scissorsImage;
		
		// Setup ImageView components
		playerImage = playerImageView;
		cpuImage = cpuImageView;
		
		// Generate a random number
		cpuChoice = rand.nextInt(SIZE);
		
		// Center output
		outputLabel.setAlignment(Pos.CENTER);
		playerResultLabel.setAlignment(Pos.CENTER);
		cpuResultLabel.setAlignment(Pos.CENTER);
		
		// Display the start message
		outputLabel.setText("Let's play!\nRock, Paper, Scissors");
		
		// Ready to play
		canPlay = true;
	}
	
	// Event listener for the newGameButton
	public void newGameButtonListener() {
		// Clear images from ImageViews
		playerImage.setImage(null);
		cpuImage.setImage(null);
		
		// Reset CPU and player choices
		cpuChoice = -1;
		playerChoice = -1;
		
		// Clear CPU and player result messages
		playerResultLabel.setText("");
		cpuResultLabel.setText("");
		
		// Generate a random number
		cpuChoice = rand.nextInt(SIZE);
		
		// Display the start message
		outputLabel.setText("Let's play!\nRock, Paper, Scissors");
		
		canPlay = true;
	}
	
	// Event listener for the rockButton
	public void rockButtonListener() {
		// Determine if ready for player choice
		if (canPlay) {
			// Set player choice
			playerChoice = 0;
			
			// Determine the winnings
			displayResults();
		} else {
			outputLabel.setText("CPU is not ready.\nPress New Game.");
		}
	}
	
	// Event listener for the paperButton
	public void paperButtonListener() {
		// Determine if ready for player choice
		if (canPlay) {
			// Set player choice
			playerChoice = 1;
			
			// Determine the winnings
			displayResults();
		} else {
			outputLabel.setText("CPU is not ready.\nPress New Game.");
		}
	}
	
	// Event listener for the scissorsButton
	public void scissorsButtonListener() {
		// Determine if ready for player choice
		if (canPlay) {
			// Set player choice
			playerChoice = 2;
			
			// Determine the winnings
			displayResults();
		} else {
			outputLabel.setText("CPU is not ready.\nPress New Game.");
		}
	}
	
	// Display results and determine winner
	public void displayResults() {
		canPlay = false;
		
		playerImage.setImage(images[playerChoice]);
		cpuImage.setImage(images[cpuChoice]);
		
		if (playerChoice == 0 && cpuChoice == 2) {
			outputLabel.setText("Rock smashes scissors.\nThe Player wins!");
			
			playerResultLabel.setText("WINNER");
			cpuResultLabel.setText("LOSER");
		} else if (playerChoice == 2 && cpuChoice == 0) {
			outputLabel.setText("Rock smashes scissors.\nThe CPU wins!");
			
			playerResultLabel.setText("LOSER");
			cpuResultLabel.setText("WINNER");
		} else if (playerChoice == 2 && cpuChoice == 1) {
			outputLabel.setText("Scissors cut paper.\nThe Player wins!");
			
			playerResultLabel.setText("WINNER");
			cpuResultLabel.setText("LOSER");
		} else if (playerChoice == 1 && cpuChoice == 2) {
			outputLabel.setText("Scissors cut paper.\nThe CPU wins!");
			
			playerResultLabel.setText("LOSER");
			cpuResultLabel.setText("WINNER");
		} else if (playerChoice == 1 && cpuChoice == 0) {
			outputLabel.setText("Paper wraps rock.\nThe Player wins!");
			
			playerResultLabel.setText("WINNER");
			cpuResultLabel.setText("LOSER");
		} else if (playerChoice == 0 && cpuChoice == 1) {
			outputLabel.setText("Paper wraps rock.\nThe CPU wins!");
			
			playerResultLabel.setText("LOSER");
			cpuResultLabel.setText("WINNER");
		} else if (playerChoice == cpuChoice) {
			outputLabel.setText("It's a draw.\nPlay again!");
			
			playerResultLabel.setText("DRAW");
			cpuResultLabel.setText("DRAW");
		}
	}
}
