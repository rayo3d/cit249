import java.io.Serializable;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-5-16
 * @purpose The CreateNumbers class is a serializable class that stores data about for the Programming Assignment 2.
 */

public class CreateNumbers implements Serializable {
	// Variable to reference an array of test scores
		private double[] numbers;
		
		public CreateNumbers(double[] t) throws IllegalArgumentException {
			// Create an array to hold the scores passed as an argument
			numbers = new double[t.length];
			
			// Copy the numbers passed as an argument into the array. Check for illegal values as they are copied.
			for (int i = 0; i < t.length; i++) {
				if (t[i] < 0 || t[i] > 100)
					throw new IllegalArgumentException("Element: " + i + " Number: " + t[i]);
				else
					numbers[i] = t[i];
			}
		}
		
		/**
		 * toString method (added for this assignment)
		 * @return A string representation of an object
		 */
		public String toString() {
			String str = "";
			
			for (int i = 0; i < numbers.length; i++)
				str += "Reading #" + i + ": " + numbers[i] + "\n";
			
			return str;
		}
}
