/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-5-16
 * @purpose This program demonstrates a solution to the Programming Assignment 2 programming challenge
 */

import java.io.*;

public class ReadNumbers {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed in the main method
		CreateNumbers[] ts = new CreateNumbers[1];
		
		try {
			// Create the file objects
			FileInputStream inputStream = new FileInputStream("NumbersList.dat");
			ObjectInputStream objectFile = new ObjectInputStream(inputStream);
			
			// Deserialize the objects and display them
			for (int i = 0; i < ts.length; i++) {
				ts[i] = (CreateNumbers) objectFile.readObject();
				System.out.println(ts[i]);
			}
			
			// Close the file
			objectFile.close();
		}
		
		catch (Exception e) {
			System.out.println("Error - " + e.getMessage());
		}
	}

}
