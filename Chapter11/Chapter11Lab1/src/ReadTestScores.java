/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-2-16
 * @purpose This program demonstrates a solution to the TestScores Modification for Serialization programming challenge
 */

import java.io.*;

public class ReadTestScores {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed in the main method
		TestScores[] ts = new TestScores[5];
		
		try {
			// Create the file objects
			FileInputStream inputStream = new FileInputStream("Objects.dat");
			ObjectInputStream objectFile = new ObjectInputStream(inputStream);
			
			// Deserialize the objects and display them
			for (int i = 0; i < ts.length; i++) {
				ts[i] = (TestScores) objectFile.readObject();
				System.out.println(ts[i]);
			}
			
			// Close the file
			objectFile.close();
		}
		
		catch (Exception e) {
			System.out.println("Error - " + e.getMessage());
		}
	}

}
