/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-5-16
 * @purpose The FileEncryptionFilter class works with files and array of text file content for the File Encryption Filter Class programming challenge.
 */

import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileEncryptionFilter {
	/**
	 * The writeArray method writes an char array to a file
	 * @param filename The name of the file
	 * @param textContent The array to write
	 * @exception IOException When an IO errors occurs
	 */
	public static void writeArray(String filename, char[] textContent) throws IOException {
		// Open the file
		FileOutputStream fstream = new FileOutputStream(filename);
		DataOutputStream outFile = new DataOutputStream(fstream);
		
		// Write the array
		for (int index = 0; index < textContent.length; index++)
			outFile.writeChar(textContent[index] + 10);
		
		// Close the file
		outFile.close();
	}
	
	/**
	 * The readArray method reads integers from a file into an char array
	 * @param filename The source file to read before encryption
	 * @exception IOException When an IO errors occurs
	 * @return textContent The array of characters 
	 */
	public static char[] readArray(File filename) throws IOException {	
		Scanner scanner = new Scanner(filename);
		String sourceFileContent = "";

		sourceFileContent = scanner.nextLine();
		while (scanner.hasNextLine()) {
			sourceFileContent = sourceFileContent + "\n" + scanner.nextLine();
		}

		char[] textContent = sourceFileContent.toCharArray();
		
		return textContent;
	}
}
