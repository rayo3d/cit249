/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-4-16
 * @purpose This program demonstrates a solution to the FileEncryption Class programming challenge
 */

import java.io.IOException;
import java.io.File;

public class FileEncryptionFilterDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables needed for main method
		File sourceFile = new File("MyLetters.txt");
		char[] source = new char[(int)sourceFile.length()];
		
		try {
			if (!sourceFile.exists()) {
		      System.out.println(args[0] + " does not exist.");
		      return;
		    }
			
			// Read the contents of the file to be encrypted into the source array
			source = FileEncryptionFilter.readArray(sourceFile);
			
			// Display processing message
			System.out.println("Encrypting the contents of the file " + sourceFile.getName() + ". The encrypted file will be stored as Encrypted.txt.\n");
			
			// Write the encrypted version of the source file to the file Encrypted.txt
			FileEncryptionFilter.writeArray("Encrypted.txt", source);
			
			// Display process complete message
			System.out.println("Done. Use Notepad to inspect the encrypted file.");
		}
		
		catch (IOException e) {
			System.out.println("Error = " + e.getMessage());
		}
	}

}
