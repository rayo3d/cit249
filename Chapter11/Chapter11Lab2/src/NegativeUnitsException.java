/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-3-16
 * @purpose 
 */

public class NegativeUnitsException extends Exception {
	/**
	 * No-arg constructor
	 */
	public NegativeUnitsException() {
		super("Negative number given for units.");
	}
	
	/**
	 * This constructor reports the units on hand that caused the exception to be thrown.
	 * @param units The invalid units on hand
	 */
	public NegativeUnitsException(int units) {
		super("Negative number given for units on hand: " + units);
	}
}
