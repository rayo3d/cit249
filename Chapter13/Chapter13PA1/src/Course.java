/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-01-16
 * @purpose 
 */

public class Course {
	// Attributes
	private String prefix;
	private int courseNo;
	private String section;
	private int enrolled;
	
	// Constructor
	public Course(String aPrefix, int aCourseNo, String aSection, int registered) {
		setPrefix(aPrefix);
		setCourseNo(aCourseNo);
		setSection(aSection);
		setEnrolled(registered);
	}
	
	// Set mutator methods with simple validation
	public void setPrefix(String aPrefix) {
		prefix = aPrefix;
	}
	
	public void setCourseNo(int aCourseNo) {
		courseNo = aCourseNo;
	}
	
	public void setSection(String aSection) {
		section = aSection;
	}
	
	public void setEnrolled(int registered) {
		enrolled = registered;
	}
	
	// Get accessor methods
	public String getPrefix() {
		return prefix;
	}
	
	public int getCourseNo() {
		return courseNo;
	}
	
	public String getSection() {
		return section;
	}
	
	public int getEnrolled() {
		return enrolled;
	}
}
