import java.util.ArrayList;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-01-16
 * @purpose Set up a structure that will download information to a dat file
 */

public class CourseData {
	// Declare items needed for CourseData class
	private static ArrayList <String> courses= new ArrayList<String>();
	private static File courseFile = new File("Course.dat");
	
	// Return curret value of ArrayList
	public static ArrayList <String> getAll() {
		try {
			FileReader fr = new FileReader(courseFile);
			BufferedReader in = new BufferedReader(fr);
			String courseNumber;
			
			while(true) {
				courseNumber = in.readLine();
				
				if(courseNumber == null) break;
				
				// create course instance and add reference to ArrayList courses
				if(courseNumber != null) {
					courses.add(courseNumber);
				}
			}//end while
		}//end try
		
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading file");
		}
		
		return courses;
	}
}
