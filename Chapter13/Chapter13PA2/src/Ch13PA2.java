import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.ArrayList;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import java.io.File;
import java.text.NumberFormat;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-13-16
 * @purpose User will be able to select options for window shades and calculate a total based on those options 
 */

public class Ch13PA2 extends JFrame {
	// Variables needing class scope
	private ArrayList <CategoryInfo>categoryStyle;
	private CategoryInfo aStyle, aSize, aColor;
	private DefaultListModel<String> categoryStyleList;
	private JList<String> categoryStyleItems;
	private ArrayList <CategoryInfo>categorySize;
	private DefaultListModel<String> categorySizeList;
	private JList<String> categorySizeItems;
	private ArrayList <CategoryInfo>categoryColor;
	private DefaultListModel<String> categoryColorList;
	private JList<String> categoryColorItems;
	private JLabel titleLabel, styleLabel, sizeLabel, colorLabel;
	private CategoryInfo aCategoryInfo;
	private JButton calculateButton, exitButton;
	private Font largeFont;
	private Border emptyBdr;
	private Border lineBorder;
	private JPanel mainPanel, top, center, bottom;
	private JMenuBar menuBar;
	private JMenu fileMenu, runMenu;
	private JMenuItem exitMenu, calculateMenu;
	private JScrollPane stylePane, sizePane, colorPane;
	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare items needed by main method
		new Ch13PA2("Shade Designer");
	}

	// Constructor
	public Ch13PA2(String titleText) {
		super(titleText);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		createLabels();
		createLists();
		createButtons();
		createPanels();
		createMenu();
		
		add(mainPanel);
		
		setSize(800,335);
		setVisible(true);
	}
	
	// Create labels then set the font on logo and align labels
	public void createLabels() {
		largeFont = new Font("Helvetica", Font.BOLD,24);
		
		titleLabel = new JLabel("Shade Designer", SwingConstants.CENTER);
		titleLabel.setFont(largeFont);
		
		styleLabel = new JLabel("Select a Style: ", SwingConstants.LEFT);
		sizeLabel = new JLabel("Select a Size: ", SwingConstants.LEFT);
		colorLabel = new JLabel("Select a Color: ", SwingConstants.LEFT);
	}
	
	// Create lists for each category
	public void createLists() {
		categorySize = ShadeData.initializeSize();
		categoryStyle = ShadeData.initializeStyle();
		categoryColor = ShadeData.initializeColor();
		
		categoryStyleList = new DefaultListModel<String>();
		categorySizeList = new DefaultListModel<String>();
		categoryColorList = new DefaultListModel<String>();
		
		categoryStyleItems = new JList<String>(categoryStyleList);
		categorySizeItems = new JList<String>(categorySizeList);
		categoryColorItems = new JList<String>(categoryColorList);
		
		categoryStyleItems.setVisibleRowCount(3);
		categorySizeItems.setVisibleRowCount(3);
		categoryColorItems.setVisibleRowCount(3);
		
		stylePane = new JScrollPane(categoryStyleItems);
		sizePane = new JScrollPane(categorySizeItems);
		colorPane = new JScrollPane(categoryColorItems);
		
		for(int i = 0; i < categoryStyle.size(); i++) {
			aStyle = (CategoryInfo) categoryStyle.get(i);
			categoryStyleList.addElement(aStyle.getCategoryDescription() + " " + currencyFormat.format(aStyle.getCategoryPrice()));
		}
		
		for(int i = 0; i < categorySize.size(); i++) {
			aSize = (CategoryInfo) categorySize.get(i);
			categorySizeList.addElement(aSize.getCategoryDescription() + " " + currencyFormat.format(aSize.getCategoryPrice()));
		}
		
		for(int i = 0; i < categoryColor.size(); i++) {
			aColor = (CategoryInfo) categoryColor.get(i);
			categoryColorList.addElement(aColor.getCategoryDescription() + " " + currencyFormat.format(aColor.getCategoryPrice()));
		}
		
		categoryStyleItems.setBorder(new CompoundBorder(new LineBorder(Color.black),new EmptyBorder(5,15,5,15)));
		categorySizeItems.setBorder(new CompoundBorder(new LineBorder(Color.black),new EmptyBorder(5,15,5,15)));
		categoryColorItems.setBorder(new CompoundBorder(new LineBorder(Color.black),new EmptyBorder(5,15,5,15)));
	}
	
	// Construct buttons and add listeners to each button
	public void createButtons() {
		Dimension size = new Dimension(195,30);
		Font buttonFont = new Font("Helvetica", Font.BOLD, 14);
		
		calculateButton = new JButton("Calculate Charges");
		calculateButton.setPreferredSize(size);
		calculateButton.setFont(buttonFont);
		
		calculateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculateCharges();
			}
		});
		
		exitButton = new JButton("Exit Program");
		exitButton.setPreferredSize(size);
		exitButton.setFont(buttonFont);
		
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}
	
	// Creates the panels with borders and add components to each panel
	public void createPanels() {
		top = new JPanel(new GridLayout(1,1,0,5));
		top.add(titleLabel);
		
		center = new JPanel();
		center.setLayout(new GridLayout(2,3,20,5));
		
		center.add(styleLabel);
		center.add(sizeLabel);
		center.add(colorLabel);
		center.add(stylePane);
		center.add(sizePane);
		center.add(colorPane);
		
		bottom = new JPanel();
		bottom.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		bottom.add(calculateButton);
		bottom.add(exitButton);
		
		mainPanel = new JPanel(new BorderLayout(10,20));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5,20,5,20));
		
		mainPanel.add(top, BorderLayout.NORTH);
		mainPanel.add(center, BorderLayout.CENTER);
		mainPanel.add(bottom, BorderLayout.SOUTH);
	}
	
	// Create window menu
	public void createMenu() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		runMenu = new JMenu("Run");
		runMenu.setMnemonic('r');
		
		menuBar.add(fileMenu);
		menuBar.add(runMenu);
		
		exitMenu = new JMenuItem("Exit");
		
		exitMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		fileMenu.add(exitMenu);
		
		calculateMenu = new JMenuItem("Calculate Charges");
		
		calculateMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculateCharges();
			}
		});
		
		runMenu.add(calculateMenu);
	}
	
	// Calculate shade total price
	public void calculateCharges() {
		int selectedStyle = categoryStyleItems.getSelectedIndex();
		int selectedSize = categorySizeItems.getSelectedIndex();
		int selectedColor = categoryColorItems.getSelectedIndex();
		
		aStyle = (CategoryInfo) categoryStyle.get(selectedStyle);
		aSize = (CategoryInfo) categorySize.get(selectedSize);
		aColor = (CategoryInfo) categoryColor.get(selectedColor);
		
		double totalCharges = aStyle.getCategoryPrice() + aSize.getCategoryPrice() + aColor.getCategoryPrice();
		
		String message = "Total charges: " + currencyFormat.format(totalCharges);
		JOptionPane.showMessageDialog(null,message,"Shade Total",JOptionPane.INFORMATION_MESSAGE);
		
		System.out.println("selectedStyle " + aStyle.getCategoryPrice());
		System.out.println("selectedSize " + aSize.getCategoryPrice());
		System.out.println("selectedColor " + aColor.getCategoryPrice());
	}
}
