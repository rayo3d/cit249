/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-13-16
 * @purpose 
 */

public class CategoryInfo {
	// Attributes
	private String categoryDescription;
	private double categoryPrice;
	
	// Constructor
	public CategoryInfo(String aCategoryDescription, double aCategoryPrice) {
		setCategoryDescription(aCategoryDescription);
		setCategoryPrice(aCategoryPrice);
	}
	
	// Set mutator methods with simple validation
	public void setCategoryDescription(String aCategoryDescription) {
		categoryDescription = aCategoryDescription;
	}
	
	public void setCategoryPrice(double aCategoryPrice) {
		categoryPrice = aCategoryPrice;
	}
	
	// Get accessor methods
	public String getCategoryDescription() {
		return categoryDescription;
	}
	
	public double getCategoryPrice() {
		return categoryPrice;
	}
}
