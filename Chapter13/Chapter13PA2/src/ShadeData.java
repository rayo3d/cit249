import java.util.ArrayList;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 3-13-16
 * @purpose Set up a structure that will download information to a dat file
 */

public class ShadeData {
	// Declare items needed for ShadeData class
	private static ArrayList <CategoryInfo> sizeCategory= new ArrayList<CategoryInfo>();
	private static File sizeFile = new File("Size.dat");
	private static ArrayList <CategoryInfo> styleCategory= new ArrayList<CategoryInfo>();
	private static File styleFile = new File("Style.dat");
	private static ArrayList <CategoryInfo> colorCategory= new ArrayList<CategoryInfo>();
	private static File colorFile = new File("Color.dat");
	
	// Read Size.dat file and assign values to class variables
	public static ArrayList <CategoryInfo> initializeSize() {
		try {
			FileReader fr = new FileReader(sizeFile);
			BufferedReader in = new BufferedReader(fr);
			String categoryDescription;
			double categoryPrice;
			
			while(true) {
				categoryDescription = in.readLine();
				
				if(categoryDescription == null) break;
				
				categoryPrice = Double.parseDouble(in.readLine());
				
				// create size category instance and add reference to ArrayList sizeCategory
				if(categoryDescription != null) {
					sizeCategory.add(new CategoryInfo(categoryDescription, categoryPrice));
				}
			}//end while
		}//end try
		
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading file");
		}
		
		return sizeCategory;
	}
	
	// Read Style.dat file and assign values to class variables
	public static ArrayList <CategoryInfo> initializeStyle() {
		try {
			FileReader fr = new FileReader(styleFile);
			BufferedReader in = new BufferedReader(fr);
			String categoryDescription;
			double categoryPrice;
			
			while(true) {
				categoryDescription = in.readLine();
				
				if(categoryDescription == null) break;
				
				categoryPrice = Double.parseDouble(in.readLine());
				
				// create size category instance and add reference to ArrayList sizeCategory
				if(categoryDescription != null) {
					styleCategory.add(new CategoryInfo(categoryDescription, categoryPrice));
				}
			}//end while
		}//end try
		
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading file");
		}
		
		return styleCategory;
	}
	
	// Read Style.dat file and assign values to class variables
	public static ArrayList <CategoryInfo> initializeColor() {
		try {
			FileReader fr = new FileReader(colorFile);
			BufferedReader in = new BufferedReader(fr);
			String categoryDescription;
			double categoryPrice;
			
			while(true) {
				categoryDescription = in.readLine();
				
				if(categoryDescription == null) break;
				
				categoryPrice = Double.parseDouble(in.readLine());
				
				// create size category instance and add reference to ArrayList sizeCategory
				if(categoryDescription != null) {
					colorCategory.add(new CategoryInfo(categoryDescription, categoryPrice));
				}
			}//end while
		}//end try
		
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error reading file");
		}
		
		return colorCategory;
	}
	
//	// Return curret value of ArrayList
//	public static ArrayList <CategoryInfo> getAll() {
//		return category;
//	}
}
