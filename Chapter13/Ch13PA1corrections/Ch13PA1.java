import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.ArrayList;

import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.util.Formatter;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */

/**
 * @author RayOsborne
 * @date 3-12-16
 * @purpose This program will let a student choose up to 4 courses for their Fall 2016 Schedule
 */

public class Ch13PA1 extends JFrame {
	// Variables needed for class
	private ArrayList <String> courses;
	private JComboBox <String> courseList;
	private JTextField firstNameField, lastNameField, studentIdField, course001Field, course002Field, course003Field, course004Field;
	private JLabel logoLabel, firstNameLabel, lastNameLabel, studentIdLabel, course001Label, course002Label, course003Label, course004Label;
	private Course aCourse;
	private JButton addCourseButton, resetButton, exitButton;
	private Font largeFont;
	private ImageIcon logo;
	private Border emptyBdr;
	private Border lineBorder;
	private Border compoundBorder;
	private JPanel mainPanel, top, dropDown, center, bottom, studenPanel, courseListPanel;
	private JSplitPane splitPane;
	private JMenuBar menuBar;
	private JMenu fileMenu, editMenu, helpMenu;
	private JMenuItem dataMenu, exitMenu, resetMenu, aboutMenu;
	private int coursesEntered = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare items needed by main method
		new Ch13PA1("Fall 2016 Course Schedule");
	}

	// Constructor
	public Ch13PA1(String titleText) {
		super(titleText);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		createIcon();
		createLabels();
		createComboBox();
		createTextFields();
		createButtons();
		createBorders();
		createPanels();
		createMenu();

		add(mainPanel);

		setSize(500,725);
		setVisible(true);
	}

	// Constructs ImageIcon
	public void createIcon() {
		logo = new ImageIcon("courses.jpg");
	}

	// Create labels then set the font on logo and align labels
	public void createLabels() {
		largeFont = new Font("Helvetic", Font.BOLD,24);

		logoLabel = new JLabel(" Fall 2016 Course Schedule", SwingConstants.CENTER);
		logoLabel.setFont(largeFont);
		logoLabel.setIcon(logo);

		firstNameLabel = new JLabel("First Name: ", SwingConstants.RIGHT);
		lastNameLabel = new JLabel("Last Name: ", SwingConstants.RIGHT);
		studentIdLabel = new JLabel("Student ID#: ", SwingConstants.RIGHT);

		course001Label = new JLabel("Course 1: ", SwingConstants.RIGHT);
		course002Label = new JLabel("Course 2: ", SwingConstants.RIGHT);
		course003Label = new JLabel("Course 3: ", SwingConstants.RIGHT);
		course004Label = new JLabel("Course 4: ", SwingConstants.RIGHT);
	}

	// Create JComboBox object
	public void createComboBox() {
		courseList = new JComboBox<String>();
		courseList.setPreferredSize(new Dimension(300, 50));
		courseList.setBorder(BorderFactory.createEmptyBorder(10,0,10,0));

		courseList.addItem("ENG 101");
		courseList.addItem("MAT 150");
		courseList.addItem("CIT 120");
		courseList.addItem("CIT 170");
		courseList.addItem("CIT 180");
		courseList.addItem("CIT 155");
		courseList.addItem("CIT 160");
		courseList.addItem("CIT 249");
	}

	// Construct buttons and add listeners to each button
	public void createButtons() {
		Dimension size = new Dimension(125,50);
		Font buttonFont = new Font("Helvetica", Font.BOLD, 14);

		addCourseButton = new JButton("Add Course");
		addCourseButton.setPreferredSize(size);
		addCourseButton.setFont(buttonFont);

		addCourseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCourseList();
			}
		});

		resetButton = new JButton("Reset");
		resetButton.setPreferredSize(size);
		resetButton.setFont(buttonFont);

		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearForms();
			}
		});

		exitButton = new JButton("Exit Program");
		exitButton.setPreferredSize(size);
		exitButton.setFont(buttonFont);

		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	// Construct text fields
	public void createTextFields() {
		firstNameField = new JTextField();
		lastNameField = new JTextField();
		studentIdField = new JTextField();
		course001Field = new JTextField();
		course002Field = new JTextField();
		course003Field = new JTextField();
		course004Field = new JTextField();
	}

	// Construct borders
	public void createBorders() {
		emptyBdr = BorderFactory.createEmptyBorder(20,40,20,40);
		lineBorder = LineBorder.createBlackLineBorder();
		compoundBorder = new CompoundBorder(lineBorder, emptyBdr);
	}

	// Creates the panels with borders and add components to each panel
	public void createPanels() {
		top = new JPanel(new GridLayout(2,1,0,20));

		dropDown = new JPanel();
		dropDown.setLayout(new FlowLayout(FlowLayout.CENTER));
		dropDown.add(courseList);

		top.add(logoLabel);
		top.add(dropDown);

		createStudenPanel();
		createCourseListPanel();

		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, studenPanel, courseListPanel);
		splitPane.setOneTouchExpandable(true);
		splitPane.setResizeWeight(0.4);

		center = new JPanel();
		center.setLayout(new GridLayout(1,1,10,10));
		center.setBorder(BorderFactory.createEmptyBorder(20,40,20,40));

		center.add(splitPane);

		bottom = new JPanel();
		bottom.setLayout(new FlowLayout(FlowLayout.CENTER));

		bottom.add(addCourseButton);
		bottom.add(resetButton);
		bottom.add(exitButton);

		mainPanel = new JPanel(new BorderLayout(10,20));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(40,40,40,40));

		mainPanel.add(top, BorderLayout.NORTH);
		mainPanel.add(center, BorderLayout.CENTER);
		mainPanel.add(bottom, BorderLayout.SOUTH);
	}

	// Setup student panel
	private void createStudenPanel() {
		studenPanel = new JPanel(new GridLayout(3,1,0,10));

		studenPanel.add(firstNameLabel);
		studenPanel.add(firstNameField);
		studenPanel.add(lastNameLabel);
		studenPanel.add(lastNameField);
		studenPanel.add(studentIdLabel);
		studenPanel.add(studentIdField);

		// Create border for the panel
		studenPanel.setBorder(new LineBorder(Color.red, 1, true));
		studenPanel.setBorder(new EmptyBorder(15,40,15,40));
	}

	// Setup course list panel
	private void createCourseListPanel() {
		courseListPanel = new JPanel(new GridLayout(4,1,0,10));

		courseListPanel.add(course001Label);
		courseListPanel.add(course001Field);
		courseListPanel.add(course002Label);
		courseListPanel.add(course002Field);
		courseListPanel.add(course003Label);
		courseListPanel.add(course003Field);
		courseListPanel.add(course004Label);
		courseListPanel.add(course004Field);

		courseListPanel.setBorder(new LineBorder(Color.green, 1, true));
		courseListPanel.setBorder(new EmptyBorder(15,40,15,40));
	}

	// Create window menu
	public void createMenu() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		editMenu = new JMenu("Edit");
		editMenu.setMnemonic('E');
		helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');

		dataMenu = new JMenuItem("Download Data");
		dataMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadCourse();
			}
		});

		fileMenu.add(dataMenu);

		exitMenu = new JMenuItem("Exit");
		exitMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		fileMenu.add(exitMenu);

		resetMenu = new JMenuItem("Reset Fields");
		resetMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearForms();
			}
		});

		editMenu.add(resetMenu);

		aboutMenu = new JMenuItem("About Program");
		aboutMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String message = "This program allows the user to plan their course schedule";
				JOptionPane.showMessageDialog(null,message,"About Program",JOptionPane.INFORMATION_MESSAGE);
			}
		});

		helpMenu.add(aboutMenu);

		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(helpMenu);
	}

	// Add course selected to course schedule fields
	private void addCourseList() {
		String selectedCourse = courseList.getItemAt(courseList.getSelectedIndex());

		String firstName = firstNameField.getText();
		String lastName = lastNameField.getText();
		String studentId = studentIdField.getText();

		if(firstName.length() == 0 || lastName.length() == 0 || studentId.length() == 0)
			JOptionPane.showMessageDialog(this, "Please Enter All Data");
		else {
			if (coursesEntered == 0) {
				course001Field.setText(selectedCourse);

				coursesEntered = 1;

				addCourseButton.setText("<html><center>Add Second Course</center></html>");
			} else if (coursesEntered == 1) {
				course002Field.setText(selectedCourse);

				coursesEntered = 2;

				addCourseButton.setText("<html><center>Add Third Course</center></html>");
			} else if (coursesEntered == 2) {
				course003Field.setText(selectedCourse);

				coursesEntered = 3;

				addCourseButton.setText("<html><center>Add Fourth Course</center></html>");
			} else if (coursesEntered == 3) {
				course004Field.setText(selectedCourse);

				coursesEntered = 4;

				addCourseButton.setText("Done");
				addCourseButton.setEnabled(false);
			}
		}
	}

	// Load ArrayList into combo box
	private void loadCourse() {
		/*courseList.removeAllItems();

		//The CourseData getAll() method is called and returns a value assigned to the courses ArrayList
		courses = CourseData.getAll();

		for(int i = 0; i < courses.size(); i++) {
			courseList.addItem(courses.get(i));
		}*/

		try
		{
			Formatter output = new Formatter(new FileOutputStream("Ch13PA1File.txt", true));
			output.format("%n%s%n", firstNameField.getText());
			output.format("%s%n", lastNameField.getText());
			output.format("%s%n", studentIdField.getText());
			output.format("%s%n", course001Field.getText());
			output.format("%s%n", course002Field.getText());
			output.format("%s%n", course003Field.getText());
			output.format("%s%n", course004Field.getText());
			output.close();
			JOptionPane.showMessageDialog(null, "Download Complete!", "Downloaded", JOptionPane.INFORMATION_MESSAGE);
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Error creating file", "File Creation Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	// Clear fields
	public void clearForms() {
		firstNameField.setText("");
		lastNameField.setText("");
		studentIdField.setText("");
		course001Field.setText("");
		course002Field.setText("");
		course003Field.setText("");
		course004Field.setText("");

		firstNameField.requestFocus();

		addCourseButton.setText("Add Course");
		addCourseButton.setEnabled(true);
	}
}
