/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-10-16
 * @purpose Program Chapter 12 Lab 3. This class inherits attributes and methods from the Horse class
 */

public class SaddleHorse extends Horse {
	private String saddleType;
	private String saddleBreed;
	
	// Constructor
	public SaddleHorse(int anIdNo, String aCoatColor, int anAge, double aHeight, String aSaddleType, String aSaddleBreed) {
		super(anIdNo, aCoatColor, anAge, aHeight);
		setSaddleType(aSaddleType);
		setSaddleBreed(aSaddleBreed);
	}
	
	// Set mutator methods alternating the values of variables
	public void setSaddleType(String aSaddleType) {
		saddleType = aSaddleType;
	}
	
	public void setSaddleBreed(String aSaddleBreed) {
		saddleBreed = aSaddleBreed;
	}
	
	// Get accessor methods
	public String getSaddleType() {
		return saddleType;
	}
	
	public String getSaddleBreed() {
		return saddleBreed;
	}
}
