/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-10-16
 * @purpose Program Chapter 12 Lab 3. This class inherits attributes and methods from the Horse class
 */

public class DraftHorse extends Horse {
	private String classification;
	private String draftBreed;
	
	// Constructor
	public DraftHorse(int anIdNo, String aCoatColor, int anAge, double aHeight, String aClassification, String aDraftBreed) {
		super(anIdNo, aCoatColor, anAge, aHeight);
		setClassification(aClassification);
		setDraftBreed(aDraftBreed);
	}
	
	// Set mutator methods alternating the values of variables
	public void setClassification(String aClassification) {
		classification = aClassification;
	}
	
	public void setDraftBreed(String aDraftBreed) {
		draftBreed = aDraftBreed;
	}
	
	// Get accessor methods
	public String getClassification() {
		return classification;
	}
	
	public String getDraftBreed() {
		return draftBreed;
	}
}
