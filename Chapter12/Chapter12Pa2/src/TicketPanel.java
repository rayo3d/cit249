import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-17-16
 * @purpose The TicketPanel class for use with the Theater Revenue programming challenge
 */

public class TicketPanel extends JPanel {
	private JTextField adultPrice; // Get adult ticket price
	private JTextField adultSold; // Get adult tickets sold
	private JTextField childPrice; // Get child ticket price
	private JTextField childSold; // Get child tickets sold
	private final double THEATER_REMAINDER = 0.8;
	
	/**
	 * Constructor
	 */
	public TicketPanel() {
		// Create a label prompting the user and a text field
		JLabel adultPriceMsg = new JLabel("Enter the adult ticket price:");
		adultPrice = new JTextField(10);
		JLabel adultSoldMsg = new JLabel("Enter the number of adult tickets sold:");
		adultSold = new JTextField(10);
		JLabel childPriceMsg = new JLabel("Enter the child ticket price:");
		childPrice = new JTextField(10);
		JLabel childSoldMsg = new JLabel("Enter the number of child tickets sold:");
		childSold = new JTextField(10);
		
		// Create a GridLayout manager
		setLayout(new GridLayout(4, 2));
		
		setBorder(new EmptyBorder(20, 20, 20, 20));
		
		// Add the text fields to the panel
		add(adultPriceMsg);
		add(adultPrice);
		add(adultSoldMsg);
		add(adultSold);
		add(childPriceMsg);
		add(childPrice);
		add(childSoldMsg);
		add(childSold);
	}
	
	/**
	 * The getAdultGross method uses the user input adult ticket price and adult number of tickets sold to return the gross sales
	 * @return The adultGross for adult ticket gross sales
	 */
	public double getAdultGross() {
		double adultGross = Double.parseDouble(adultPrice.getText()) * Integer.parseInt(adultSold.getText());
		return adultGross;
	}
	
	/**
	 * The getAdultNet method uses the adult gross sales and calculate theater's portion
	 * @param adultGrossSales The adult gross sales
	 * @return The adultNet for adult ticket net sales
	 */
	public double getAdultNet(double adultGrossSales) {
		double adultNet = adultGrossSales * THEATER_REMAINDER;
		return adultNet;
	}
	
	/**
	 * The getChildGross method uses the user input child ticket price and child number of tickets sold to return the gross sales
	 * @return The childGross for child ticket gross sales
	 */
	public double getChildGross() {
		double childGross = Double.parseDouble(childPrice.getText()) * Integer.parseInt(childSold.getText());
		return childGross;
	}
	
	/**
	 * The getChildNet method uses the child gross sales and calculate theater's portion
	 * @param childGrossSales The child gross sales
	 * @return The childNet for child ticket net sales
	 */
	public double getChildNet(double childGrossSales) {
		double childNet = childGrossSales * THEATER_REMAINDER;
		return childNet;
	}
	
	/**
	 * The getTotalGross method uses the adult gross sales and child gross sales to calculate total gross
	 * @param adultGrossSales The adult gross sales
	 * @param childGrossSales The child gross sales
	 * @return The totalGross for total gross sales
	 */
	public double getTotalGross(double adultGrossSales, double childGrossSales) {
		double totalGross = adultGrossSales + childGrossSales;
		return totalGross;
	}
	
	/**
	 * The getTotalNet method uses the adult gross net and child net sales to calculate total net
	 * @param adultNetSales The adult net sales
	 * @param childNetSales The child net sales
	 * @return The totalNet for total net sales
	 */
	public double getTotalNet(double adultNetSales, double childNetSales) {
		double totalNet = adultNetSales + childNetSales;
		return totalNet;
	}
	
	// Clear fields and reset radio buttons
	public void clearTickets() {
		adultPrice.setText("");
		adultSold.setText("");
		childPrice.setText("");
		childSold.setText("");
		adultPrice.requestFocus();
	}
}
