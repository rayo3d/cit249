import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-17-16
 * @purpose Chapter 12 PA 2. This program will take user input wholesale cost and markup percentage of an item and display the final retail price.
 */

public class Ch12Pa2 extends JFrame {
	private TicketPanel ticketPanel; // A panel for item information
	private JPanel buttonPanel; // A panel for the buttons
	private JButton calcButton; // Calculates everything
	private JButton resetButton; // Resets the text fields
	
	/**
	 * Constructor
	 */
	public Ch12Pa2() {
		// Display a title
		setTitle("Theater Revenue");
		
		// Specify what happens when the close button is clicked
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Create a TicketPanel object
		ticketPanel = new TicketPanel();
		
		// Build the panel that contains the buttons
		buildButtonPanel();
		
		// Add the panels to the content pane
		add(ticketPanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);
		
		// Pack and display the window
		pack();
		setVisible(true);
	}
	
	/** 
	 * The buildButtonPanel method creates a panel containing buttons
	 */
	private void buildButtonPanel() {
		// Create a button to calculate the charges
		calcButton = new JButton("Calculate Revenue");
		
		// Add an action listener to the button
		calcButton.addActionListener(new CalcButtonListener());
		
		// Create a button to exit the application
		resetButton = new JButton("Reset");
		
		// Add an action listener to the button
		resetButton.addActionListener(new ResetButtonListener());
		
		// Put the buttons in their own panel
		buttonPanel = new JPanel();
		buttonPanel.add(calcButton);
		buttonPanel.add(resetButton);
	}
	
	/**
	 * CalcButtonListener is an action listener class for the calcButton component
	 */
	private class CalcButtonListener implements ActionListener {
		/**
		 * actionPerformed method
		 * @param e An ActionEvent object
		 */
		public void actionPerformed(ActionEvent e) {
			double adultGrossSales; // Total adult gross sales
			double adultNetSales; // Total adult net sales
			double childGrossSales; // Total child gross sales
			double childNetSales; // Total child net sales
			double totalGrossSales; // Total ticket gross sales
			double totalNetSales; // Total ticket net sales
			NumberFormat currency = NumberFormat.getCurrencyInstance(); // Formats pricing
			
			// Get the total charges
			adultGrossSales = ticketPanel.getAdultGross();
			adultNetSales = ticketPanel.getAdultNet(adultGrossSales);
			childGrossSales = ticketPanel.getChildGross();
			childNetSales = ticketPanel.getChildNet(childGrossSales);
			totalGrossSales = ticketPanel.getTotalGross(adultGrossSales, childGrossSales);
			totalNetSales = ticketPanel.getTotalNet(adultNetSales, childNetSales);
			
			// Display the message
			JOptionPane.showMessageDialog(null, String.format(
					"Gross revenue for adult tickets sold: " + currency.format(adultGrossSales) +
					"\nNet revenue for adult tickets sold: " + currency.format(adultNetSales) +
					"\nGross revenue for child tickets sold: " + currency.format(childGrossSales) +
					"\nNet revenue for child tickets sold: " + currency.format(childNetSales) +
					"\nTotal gross revenue: " + currency.format(totalGrossSales) +
					"\nTotal net revenue: " + currency.format(totalNetSales)
					));
		}
	} // End of inner class
	
	/**
	 * ResetButtonListener is an action listener class for the resetButton component
	 */
	private class ResetButtonListener implements ActionListener {
		/**
		 * actionPerformed method
		 * @param e An ActionEvent object
		 */
		public void actionPerformed(ActionEvent e) {
			ticketPanel.clearTickets();
		}
	} // End of inner class

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Ch12Pa2 ld = new Ch12Pa2();
	}

}
