import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.CompoundBorder;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-9-16
 * @purpose 
 */

public class Ch12Lab1 extends JFrame implements ActionListener {
	// Declare components needed for this class
	JButton buttons[];
	JPanel topPanel, centerPanel, bottomPanel, hiddenPanel;
	JLabel topLabel, bottomLabel, hiddenLabel;
	JTextField topField;
	JRadioButton[] RBs;
	ButtonGroup RBGroup;
	Border emptyBdr = BorderFactory.createEmptyBorder(20, 20, 20, 20);
	Border lineBorder = LineBorder.createBlackLineBorder();
	Border compoundBorder = new CompoundBorder(lineBorder, emptyBdr);

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == RBs[0]) {
			topPanel.remove(hiddenLabel);
			topPanel.setBorder(emptyBdr);
			topPanel.revalidate();
			topPanel.repaint();
			
			topPanel.add(topLabel);
			topPanel.add(topField);
		}
		
		if (arg0.getSource() == RBs[1]) {
			topPanel.remove(topLabel);
			topPanel.remove(topField);
			topPanel.add(hiddenLabel);
			topPanel.setBorder(compoundBorder);
			topPanel.revalidate();
			topPanel.repaint();
		}
		
		String arg = arg0.getActionCommand();
		
		if(arg == "Clear") {
			topField.setText("");
			topField.setBackground(Color.white);
		}
		if (arg == "Exit") {
			System.exit(0);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare components needed for main method
		JFrame.setDefaultLookAndFeelDecorated(true);
		Ch12Lab1 frameInfo = new Ch12Lab1();
		
		frameInfo.setSize(350,250);
		frameInfo.setVisible(true);
		frameInfo.setTitle("Chapter 12 Lab 1");
	}

	// Constructor method
	public Ch12Lab1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		createLabels();
		createTextField();
		createButtons();
		createRadioButtons();
		createPanels();
		addPanelComponents();
	}
	
	// Construct labels
	public void createLabels() {
		// Construct the labels
		hiddenLabel = new JLabel("Now something different displays", SwingConstants.CENTER);
		topLabel = new JLabel("Testing Label    ", SwingConstants.RIGHT);
	}
	
	// Construct text fields
	public void createTextField() {
		// construct the text field
		topField = new JTextField();
	}
	
	// Construct buttons
	public void createButtons() {
		// construct the buttons
		buttons = new JButton[2];
		buttons[0] = new JButton("Clear");
		buttons[1] = new JButton("Exit");
	}
	
	// Construct radio buttons
	public void createRadioButtons() {
		// construct the radio buttons
		RBGroup = new ButtonGroup();
		RBs = new JRadioButton[2];
		
		RBs[0] = new JRadioButton("Show Labels/Fields", true);
		RBs[1] = new JRadioButton("Hide Labels/Fields", false);
		
		for (int i = 0; i < RBs.length; i++) {
			RBGroup.add(RBs[i]);
		}
	}
	
	// Construct panels
	public void createPanels() {
		// construct the different panels
		topPanel = new JPanel();
		topPanel.setBorder(emptyBdr);
		topPanel.setLayout(new GridLayout(1, 2));
		
		topPanel.add(topLabel);
		topPanel.add(topField);
		
		centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(1, 2));
		
		for (int i = 0; i < RBs.length; i++)
			centerPanel.add(RBs[i]);
		
		bottomPanel = new JPanel();
		
		for (int i = 0; i < buttons.length; i++)
			bottomPanel.add(buttons[i]);
		
		hiddenPanel = new JPanel(new GridLayout(1, 2));
	}
	
	// Construct panel and framePanel
	public void addPanelComponents() {
		for (int i = 0; i < RBs.length; i++) {
			centerPanel.add(RBs[i]);
			RBs[i].addActionListener(this);
		}
		
		// add the buttons to the bottom panel
		for (int i = 0; i < buttons.length; i++) {
			bottomPanel.add(buttons[i]);
			buttons[i].addActionListener(this);
		}
		
		add(topPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
	}
}
