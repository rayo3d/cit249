import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-16-16
 * @purpose The ItemPanel class for use with the Retail Price Calculator programming challenge
 */

public class ItemPanel extends JPanel {
	private JTextField itemCost; // Get wholesale cost
	private JTextField itemMarkup; // Get markup percentage
	
	/**
	 * Constructor
	 */
	public ItemPanel() {
		// Create a label prompting the user and a text field
		JLabel itemCostMsg = new JLabel("Enter the wholesale cost:");
		itemCost = new JTextField(10);
		JLabel itemMarkupMsg = new JLabel("Enter the markup percentage:");
		itemMarkup = new JTextField(10);
		
		// Create a GridLayout manager
		setLayout(new GridLayout(2, 2));
		
		setBorder(new EmptyBorder(20, 20, 20, 20));
		
		// Add the text fields to the panel
		add(itemCostMsg);
		add(itemCost);
		add(itemMarkupMsg);
		add(itemMarkup);
	}
	
	/**
	 * The getPrice method uses the user input wholesale cost and markup percentage to calculate the final retail price
	 * @return The retailPrice for item
	 */
	public double getPrice() {
		double retailPrice = Integer.parseInt(itemCost.getText()) + (Integer.parseInt(itemCost.getText()) * Double.parseDouble(itemMarkup.getText()));
		return retailPrice;
	}
}
