import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 2-16-16
 * @purpose Chapter 12 PA 1. This program will take user input wholesale cost and markup percentage of an item and display the final retail price.
 */

public class Ch12Pa1 extends JFrame {
	private ItemPanel itemPanel; // A panel for item information
	private JPanel buttonPanel; // A panel for the buttons
	private JButton calcButton; // Calculates everything
	private JButton exitButton; // Exits the application
	
	/**
	 * Constructor
	 */
	public Ch12Pa1() {
		// Display a title
		setTitle("Retail Price Calculator");
		
		// Specify what happens when the close button is clicked
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Create a ItemPanel object
		itemPanel = new ItemPanel();
		
		// Build the panel that contains the buttons
		buildButtonPanel();
		
		// Add the panels to the content pane
		add(itemPanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);
		
		// Pack and display the window
		pack();
		setVisible(true);
	}
	
	/** 
	 * The buildButtonPanel method creates a panel containing buttons
	 */
	private void buildButtonPanel() {
		// Create a button to calculate the charges
		calcButton = new JButton("Calculate Charges");
		
		// Add an action listener to the button
		calcButton.addActionListener(new CalcButtonListener());
		
		// Create a button to exit the application
		exitButton = new JButton("Exit");
		
		// Add an action listener to the button
		exitButton.addActionListener(new ExitButtonListener());
		
		// Put the buttons in their own panel
		buttonPanel = new JPanel();
		buttonPanel.add(calcButton);
		buttonPanel.add(exitButton);
	}
	
	/**
	 * CalcButtonListener is an action listener class for the calcButton component
	 */
	private class CalcButtonListener implements ActionListener {
		/**
		 * actionPerformed method
		 * @param e An ActionEvent object
		 */
		public void actionPerformed(ActionEvent e) {
			double finalRetailPrice; // Total charges
			
			// Get the total charges
			finalRetailPrice = itemPanel.getPrice();
			
			// Display the message
			JOptionPane.showMessageDialog(null, String.format("The retail price is $%,.2f", finalRetailPrice));
		}
	} // End of inner class
	
	/**
	 * ExitButtonListener is an action listener class for the exitButton component
	 */
	private class ExitButtonListener implements ActionListener {
		/**
		 * actionPerformed method
		 * @param e An ActionEvent object
		 */
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	} // End of inner class

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Ch12Pa1 ld = new Ch12Pa1();
	}

}
