import java.util.Scanner;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 4-2-16
 * @purpose This program demonstrates a solution to the Recursive Multiplication programming challenge
 */

public class Ch16PA1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declarations needed for main method
		double num1, num2; // Numbers to be recursively multiplied
		Scanner keyboard = new Scanner(System.in); // Crease a Scanner object for keyboard input
		
		// Get the first number to multiply
		System.out.print("Enter a number: ");
		num1 = keyboard.nextDouble();
		
		// Get the second number to multiply
		System.out.print("Enter another number: ");
		num2 = keyboard.nextDouble();
		
		keyboard.close();
		
		displayResult(num1, num2);
	}
	
	/*
	 * The displayResult method displays input numbers and the result of the recursive multiplication operation
	 * @param numOne The count to multiply a number
	 * @param numTwo The number that should be multiplied
	 * 
	 */
	public static void displayResult (double numOne, double numTwo) {
		double result = multiply(numOne, 0.0, numTwo);
		
		System.out.println(numOne + " times " + numTwo + " equals " + result);
	}

	/*
	 * The multiply method performs the recursive multiplication operation
	 * @param numNew The updated result
	 * @param numUse The number that should be multiplied
	 * @return The result of recursive multiplication operation
	 */
	public static double multiply (double counter, double numNew, double numUse) {		
		if (counter < 0) {
			counter = Math.abs(counter);
			numUse = -numUse;
		} else if (counter > 0) {
			counter--;

	        return multiply(counter, numNew + numUse, numUse);
		}

		return numNew;
	}
}
