import java.util.Scanner;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 4-26-16
 * @purpose This program demonstrates a solution to the Recursive Power Method programming challenge
 */

public class Ch16PA2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declarations needed for main method
		double num; // Number to be raised
		double power; // Exponent to raise power by
		Scanner keyboard = new Scanner(System.in); // Crease a Scanner object for keyboard input
		
		// Get the first number to multiply
		System.out.print("Enter a number: ");
		num = keyboard.nextDouble();
		
		// Get the second number to multiply
		System.out.print("Enter an exponent: ");
		power = keyboard.nextDouble();
		
		// Validate the input
		while (power < 0) {
			System.out.print("Invalid. Enter a positive exponent: ");
			power = keyboard.nextDouble();
		}
		
		keyboard.close();
		
		displayResult(num, power);
	}

	/*
	 * The displayResult method displays input numbers and the result of the recursive multiplication operation
	 * @param number Number to be raised
	 * @param exponent Exponent to raise power by
	 * 
	 */
	public static void displayResult (double number, double exponent) {
		double result = raiseByPower(exponent, number, exponent);
		
		System.out.println(number + " times " + exponent + " equals " + result);
	}

	/*
	 * The multiply method performs the recursive multiplication operation
	 * @param numNew The updated result
	 * @param powerBy The number that should be multiplied
	 * @return The result of recursive multiplication operation
	 */
	public static double raiseByPower (double counter, double numNew, double powerBy) {
		if (powerBy == 0)
			return 1.0;
		
		if (counter > 1) {
			counter--;
			
			numNew += numNew;

	        return raiseByPower(counter, numNew, powerBy);
		}

		return numNew;
	}
}
