import java.util.Scanner;

/**
 * 
 */

/**
 * @author RayOsborne
 * @date 4-25-16
 * @purpose This program demonstrates a solution to the isMember Method programming challenge
 */

public class Ch16Lab1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declarations needed for main method
		final int ARRAY_SIZE = 10;
		int[] numbers = new int[ARRAY_SIZE];
		
		Scanner keyboard = new Scanner(System.in);
		
		// Get user input for numbers
		System.out.println("Enter " + ARRAY_SIZE + " numbers:");
		
		for (int i=0; i < numbers.length; i++) {
			numbers[i] = keyboard.nextInt();
		}
		
		keyboard.close();
		
		// Test all of the values 0 through 20 and see if they are in the array
		for (int x = 0; x <= 20; x++) {
			if (isMember(numbers, x, ARRAY_SIZE))
				System.out.println(x + " is found in the array.\n");
			else
				System.out.println(x + " is not found in the array.\n");
		}
	}

	/*
	 * The isMember method searches all or part of an array for value
	 * @param array The array to be searched
	 * @param value The value to search for
	 * @param size The size of the part of the array being searched
	 * @return if the value is found, the method returns true. Otherwise, it returns false
	 */
	public static boolean isMember(int[] array, int value, int size) {
		if (size == 0)
			return false;
		else if (array[size - 1] == value)
			return true;
		else
			return isMember(array, value, size - 1);
	}
}
