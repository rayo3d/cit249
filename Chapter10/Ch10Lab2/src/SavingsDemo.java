/**
 * 
 */

/**
 * @author RayOsborne
 * @date 1-20-16
 * @purpose This program demonstrates a solution to the BankAccount and SavingsAccount classes programming challenge.
 */

import java.text.NumberFormat;

public class SavingsDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables needed for main method
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		
		// Create a SavingsAccount object with a $100 balance, 3% interest rate, and a monthly service charge of $2.50
		SavingsAccount savings = new SavingsAccount(100.0, 0.03, 2.50);
		
		// Display SavingsAccount
		System.out.println("Balance: " + currency.format(savings.getBalance()));
		System.out.println("Number of deposits: " + savings.getNumDeposits());
		System.out.println("Number of withdrawals: " + savings.getNumWithdrawals());
		System.out.println();
		
		// Make some deposits
		savings.deposit(25.00);
		savings.deposit(10.00);
		savings.deposit(35.00);
		
		// Display updated SavingsAccount
		System.out.println("Balance: " + currency.format(savings.getBalance()));
		System.out.println("Number of deposits: " + savings.getNumDeposits());
		System.out.println("Number of withdrawals: " + savings.getNumWithdrawals());
		System.out.println();
		
		// Make some withdrawals
		savings.withdraw(100.00);
		savings.withdraw(50.00);
		savings.withdraw(10.00);
		savings.withdraw(1.00);
		savings.withdraw(1.00);
		
		// Display updated SavingsAccount
		System.out.println("Balance: " + currency.format(savings.getBalance()));
		System.out.println("Number of deposits: " + savings.getNumDeposits());
		System.out.println("Number of withdrawals: " + savings.getNumWithdrawals());
		System.out.println();
		
		// Do the monthly processing
		savings.monthlyProcess();
		
		// Display updated SavingsAccount
		System.out.println("Balance: " + currency.format(savings.getBalance()));
		System.out.println("Number of deposits: " + savings.getNumDeposits());
		System.out.println("Number of withdrawals: " + savings.getNumWithdrawals());
		System.out.println();
	}

}
