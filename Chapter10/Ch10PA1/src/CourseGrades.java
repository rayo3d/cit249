/**
 * 
 */

/**
 * @author RayOsborne
 * @date 1-21-16
 * @purpose The CourseGrades class will set the grades for various assignments and return the grade information.
 */

public class CourseGrades {
	// Declare variables for CourseGrades class
	private GradedActivity[] grades = new GradedActivity[4];
	
	// Constructor method 
	public CourseGrades() {
		
	}
	
	/**
	 * The setLab method sets the lab grade item.
	 * @param lab The lab activity. 
	 */
	public void setLab(GradedActivity lab) {
		grades[0] = lab;
	}
	
	/**
	 * The setPassFailExam method sets the PassFailExam grade item.
	 * @param passFailExam The PassFailExam activity. 
	 */
	public void setPassFailExam(PassFailExam passFailExam) {
		grades[1] = passFailExam;
  	}
	
	/**
	 * The setEssay method sets the Essay grade item.
	 * @param essay The Essay activity. 
	 */
	public void setEssay(Essay essay) {
		grades[2] = essay;
	}
	
	/**
	 * The setFinalExam method sets the FinalExam grade item.
	 * @param finalExam The FinalExam activity. 
	 */
	public void setFinalExam(FinalExam finalExam) {
		grades[3] = finalExam;
	}
	
	
	/**
	 * toString method
	 * @return A reference to a String representation of the object
	 */
	public String toString() {
		String str = "Lab Score: " + grades[0].getScore() + " Grade: " + grades[0].getGrade();
		str += "\nPass/Fail Exam Score: " + grades[1].getScore() + " Grade: " + grades[1].getGrade();
		str += "\nEssay Score: " + grades[2].getScore() + " Grade: " + grades[2].getGrade();
		str += "\nFinal Exam Score: " + grades[3].getScore() + " Grade: " + grades[3].getGrade();
		
		return str;
	}
}
