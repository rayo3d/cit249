/**
 * 
 */

/**
 * @author RayOsborne
 * @date 1-21-16
 * @purpose The CourseGradesDemo class executes and demos functionality for the Course Grades programming challenge.
 */

import java.util.Scanner;

public class CourseGradesDemo {
	public static void main(String[] args) {
		// Declare variables needed for main method
		Scanner keyboard = new Scanner(System.in);
		final int PASS_FAIL_QS = 10;
		final double PASS_FAIL_MIN = 70.0;
		final int FINAL_QS = 50;
		double labScore;
		int examMissed;
		double grammar, spelling, length, content;
		int finalMissed;
		
		// Capture student scores
		System.out.print("What is the student's lab score? ");
		labScore = keyboard.nextDouble();
		System.out.println();
		
		System.out.print("How many questions did the student miss on the pass/fail exam? ");
		examMissed = keyboard.nextInt();
		System.out.println();
		
		System.out.print("What is the student's essay grammar score? ");
		grammar = keyboard.nextDouble();
		System.out.print("What is the student's essay spelling score? ");
		spelling = keyboard.nextDouble();
		System.out.print("What is the student's essay length score? ");
		length = keyboard.nextDouble();
		System.out.print("What is the student's essay content score? ");
		content = keyboard.nextDouble();
		System.out.println();
		
		System.out.print("How many questions did the student miss on the final exam? ");
		finalMissed = keyboard.nextInt();
		System.out.println();
		
		// Create CourseGrades object and set scores
		CourseGrades courseGrades = new CourseGrades();
		GradedActivity lab = new GradedActivity();
		lab.setScore(labScore);
		PassFailExam passFailExam = new PassFailExam(PASS_FAIL_QS, examMissed, PASS_FAIL_MIN);
		Essay essay = new Essay();
		essay.setScore(grammar, spelling, length, content);
		FinalExam finalExam = new FinalExam(FINAL_QS, finalMissed);
		
		// Populate courseGrades object
		courseGrades.setLab(lab);
		courseGrades.setPassFailExam(passFailExam);
		courseGrades.setEssay(essay);
		courseGrades.setFinalExam(finalExam);
		
		// Display results
		System.out.println(courseGrades);
	}
}
