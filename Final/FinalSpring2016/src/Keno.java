import javax.swing.JFrame;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.BorderFactory;
import javax.swing.Box;

import java.awt.Font;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.border.TitledBorder;

import java.text.NumberFormat;
import javax.swing.Timer;

/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 3-16-16
 * @purpose This program will be a functional Keno game.
 */

public class Keno extends JFrame {
	private static TitledBorder radioBorder = new TitledBorder("Choose Bet Amount ($)");
	private static TitledBorder spotsBorder = new TitledBorder("Choose Numbers to Play");
	private static JButton[] buttons = new JButton[80];
	private static TopPanel buttonPanel1;
	private static BottomPanel buttonPanel2;
	private static MiddlePanel middlePanel;
	private static ButtonGroup dollarGroup = new ButtonGroup();
	private static ButtonGroup spotsGroup = new ButtonGroup();
	private static JRadioButton[] dollars = new JRadioButton[6];
	private static JRadioButton[] spots = new JRadioButton[12];
	private static Font font = new Font("Arial", Font.BOLD, 38);
	private static Font font2 = new Font("Arial", Font.BOLD, 18);
	private static JLabel luckyLabel = new JLabel(new ImageIcon("images/Lucky.png"));
	
	private int count = 0;
	private int correct = 0;
	private int numbersBet = 0;
	private int amountBet = 0;
	private double amountWon = 0.0;
	private int counter = 0;
	private int i = 0;
	private NumberFormat currency = NumberFormat.getCurrencyInstance();
	private JButton betMaxButton = new JButton("Bet Max Amount");
	private JButton eraseButton = new JButton("Clear Board");
	private JButton startButton = new JButton("Generate Numbers");
	private ImageIcon icon = new ImageIcon("images/checkmark.gif");
	private ArrayList<String> picks = new ArrayList<String>();
	private ArrayList<Integer> shuffleNumbers = new ArrayList<Integer>();
	private ArrayList<Integer> winningNumbers = new ArrayList<Integer>();
	private JMenuBar menuBar;
	private JMenu file = new JMenu("File");
	private JMenu help = new JMenu("Help");
	private JMenu payScale = new JMenu("Payoffs");
	private JMenuItem exit = new JMenuItem("Exit Program");
	private JMenuItem info = new JMenuItem("How to Play Keno");
	private JMenuItem[] payoffs = new JMenuItem[12];
	private Color color;
	private Timer timer;
	
	// Constructor
	public Keno() {
		// Set layout for frame
		setLayout(new GridLayout(3, 1));
	    
		// Call methods to create frame objects
		createMenu();
	    createButtons();
	    createRadioButtons();
	    createPanels();
	    populateArrayList();
	    
	    // Add panels to frame
	    add(buttonPanel1);
	    add(middlePanel);
	    add(buttonPanel2);
	}
	
	// Create the menu bar
	public void createMenu() {
		// Set the window's menu bar.
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// Add the menus to the menu bar.
		menuBar.add(file);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(betMaxButton);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(eraseButton);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(startButton);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(payScale);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(help);
		menuBar.add(Box.createHorizontalGlue());
		
		// Add sub-menus to menu items
		file.add(exit);
		help.add(info);
		
		for (int i = 0; i < payoffs.length; i++) {
			payoffs[i] = new JMenuItem(String.valueOf(i + 1) + " Spots Selected");
			
			payScale.add(payoffs[i]);
			
			payoffs[i].setFont(font2);
		}
		
		for (int i = 0; i < payoffs.length; i++) {
			payoffs[i].setHorizontalAlignment(SwingConstants.RIGHT);
		}
		
		// Assign Font to menu items
		file.setFont(font2);
		payScale.setFont(font2);
		help.setFont(font2);
		exit.setFont(font2);
		info.setFont(font2);
		
		// Set Mnemonics
		file.setMnemonic('F');
		help.setMnemonic('H');
		exit.setMnemonic('E');
		info.setMnemonic('K');
		payScale.setMnemonic('P');
		
		// Add Listener to items
		info.addActionListener(new Listener());
		exit.addActionListener(new Listener());
		betMaxButton.addActionListener(new Listener());
		eraseButton.addActionListener(new Listener());
		startButton.addActionListener(new Listener());
		
		// Assign Font to menu buttons
		betMaxButton.setFont(font2);
		eraseButton.setFont(font2);
		startButton.setFont(font2);
		
		// Add Listener to payoffs array
		for (int i = 0; i < payoffs.length; i++) {
			payoffs[i].addActionListener(new Listener());
		}
	}
	
	// Create all buttons for game
	public void createButtons() {
		for (int i = 0; i < buttons.length; i++) {
			buttons[i] = new JButton(String.valueOf(i + 1));
			
			buttons[i].addActionListener(new ButtonListener());
			
			buttons[i].setHorizontalAlignment(JButton.CENTER);
			
			buttons[i].setFont(font);
			
			buttons[i].setBackground(Color.BLUE);
			buttons[i].setForeground(Color.YELLOW);
		}
	}
	
	// Create all radio buttons for game
	public void createRadioButtons() {
		// Create dollars radio buttons
		dollars[0] = new JRadioButton("1");
		dollars[1] = new JRadioButton("2");
		dollars[2] = new JRadioButton("3");
		dollars[3] = new JRadioButton("5");
		dollars[4] = new JRadioButton("10");
		dollars[5] = new JRadioButton("20");
		
		for (int i = 0; i < dollars.length; i++) {
			dollars[i].setFont(font2);
			
			dollars[i].addItemListener(new RadioListener());
			
			dollarGroup.add(dollars[i]);
		}
		
		// Create spots radio buttons
		for (int i = 0; i < spots.length; i++) {
			spots[i] = new JRadioButton(String.valueOf(i + 1));
			
			spots[i].setFont(font2);
			
			spots[i].addItemListener(new RadioListener());
			
			spotsGroup.add(spots[i]);
		}
	}
	
	// Create panels
	public void createPanels() {
		buttonPanel1 = new TopPanel();
		buttonPanel2 = new BottomPanel();
		middlePanel = new MiddlePanel();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Set frame attributes
		JFrame.setDefaultLookAndFeelDecorated(true);
		Keno frame = new Keno();
		frame.setSize(900, 750);
		frame.setTitle("Let's Play Keno");
		frame.setVisible(true);
	}

	// TopPanel class
	class TopPanel extends JPanel {
		// Constructor
		TopPanel() {
			// Set layout for TopPanel
			setLayout(new GridLayout(4, 10));
			
			// Add buttons to TopPanel
			for (int i = 0; i < 40; i++) {
				add(buttons[i]);
			}
		}
	}
	
	// BottomPanel class
	class BottomPanel extends JPanel {
		// Constructor
		BottomPanel() {
			// Set layout for TopPanel
			setLayout(new GridLayout(4, 10));
			
			// Add buttons to TopPanel
			for (int i = 40; i < buttons.length; i++) {
				add(buttons[i]);
			}
		}
	}
	
	// MiddlePanel class
	class MiddlePanel extends JPanel {
		// Declare objects for MiddlePanel
		private JPanel dollarPanel = new JPanel(new GridLayout(6, 1));
		private JPanel spotsPanel = new JPanel(new GridLayout(6, 2));
		
		// Default constructor
		MiddlePanel() {
			// Set middle panel attributes
			setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
			setBackground(Color.white);
			
			// Set dollarPanel attributes
			dollarPanel.setBorder(radioBorder);
			dollarPanel.setPreferredSize(new Dimension(175, 200));
			
			// Add elements to dollarPanel
			for (int i = 0; i < dollars.length; i++) {
				dollarPanel.add(dollars[i]);
			}
			
			// Set spotsPanel attributes
			spotsPanel.setBorder(spotsBorder);
			spotsPanel.setPreferredSize(new Dimension(175, 200));
			
			// Add elements to spotsPanel
			spotsPanel.add(spots[0]);
			spotsPanel.add(spots[6]);
			spotsPanel.add(spots[1]);
			spotsPanel.add(spots[7]);
			spotsPanel.add(spots[2]);
			spotsPanel.add(spots[8]);
			spotsPanel.add(spots[3]);
			spotsPanel.add(spots[9]);
			spotsPanel.add(spots[4]);
			spotsPanel.add(spots[10]);
			spotsPanel.add(spots[5]);
			spotsPanel.add(spots[11]);
			
			add(spotsPanel);
			add(luckyLabel);
			add(dollarPanel);
		}
	}
	
	public void clear() {
		winningNumbers.clear();
		winningNumbers.trimToSize();
		
		count = 0;
		correct = 0;
		amountWon = 0;
		i = 0;
		
		for (i = 0; i < buttons.length; i++) {
			buttons[i].setText(String.valueOf(i + 1));
			
			buttons[i].setBackground(Color.BLUE);
			buttons[i].setForeground(Color.YELLOW);
			
			buttons[i].setIcon(null);
		}
		
		dollarGroup.clearSelection();
		spotsGroup.clearSelection();
		
		numbersBet = 0;
		amountBet = 0;
	}
	
	public void generateNumbers() {
		winningNumbers.clear();
		winningNumbers.trimToSize();

		Collections.shuffle(shuffleNumbers);
		
		if (numbersBet <= 0) {
			JOptionPane.showMessageDialog(null, "Choose numbers to play");
			
			clear();
		} else if (count != numbersBet) {
			JOptionPane.showMessageDialog(null, "You must select " + numbersBet + " numbers");
			
			clear();
		} else {
			for (i = 0; i < 20; i++) {
				winningNumbers.add(shuffleNumbers.get(i));
			}
			
			timer = new Timer (3000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (i < buttons.length) {
						if (buttons[winningNumbers.get(i)].getIcon() != null) {
							correct++;
							
							buttons[winningNumbers.get(i)].setBackground(Color.GREEN);
						} else {
							buttons[winningNumbers.get(i)].setBackground(Color.RED);
						}
					}
					
					if (i < 19) {
						i++;
					} else {
						((Timer)e.getSource()).stop();
						
						// Notify the user that the download is complete
						JOptionPane.showMessageDialog(null, "The numbers you have selected and the numbers generated, have been successfully downloaded");
						
						displayWinnings();
					}
				}
			});
			
			if (!timer.isRunning())
				timer.start();
			
			try {
				Formatter output = new Formatter(new FileOutputStream("Final.txt", true));
				Formatter userPicks = new Formatter(new FileOutputStream("UserPicks.txt", true));
				
				for (i = 0; i < 20; i++) {
					output.format("%d ", (winningNumbers.get(i) + 1));
				}
				
				for (i = 0; i < picks.size(); i++) {
					userPicks.format("%s ", picks.get(i));
				}
				
				output.close();
				userPicks.close();
			}
			catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Error creating file");
			}
		}
	}
	
	public void displayWinnings() {
		switch(numbersBet) {
			case 1:
				switch(correct) {
					case 1:
						amountWon = 2.5 * amountBet;
						break;
					case 0:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers. Sorry but you must match a number to win.");
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers. Sorry but you must match a number to win.");
						break;
				}
				break;
			case 2:
				switch(correct) {
					case 1:
						amountWon = 1 * amountBet;
						break;
					case 2:
						amountWon = 5 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers. Sorry but you must match a number to win.");
						break;
				}
				break;
			case 3:
				switch(correct) {
					case 2:
						amountWon = 2.50 * amountBet;
						break;
					case 3:
						amountWon = 25 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least 2 numbers to win.");
						break;
				}
				break;

			case 4:
				switch(correct) {
					case 2:
						amountWon = 1 * amountBet;
						break;
					case 3:
						amountWon = 4 * amountBet;
						break;
					case 4:
						amountWon = 100 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least two numbers to win.");
						break;
				}
				break;
			case 5:
				switch(correct) {
					case 3:
						amountWon = 2 * amountBet;
						break;
					case 4:
						amountWon = 20 * amountBet;
						break;
					case 5:
						amountWon = 450 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least three numbers to win.");
						break;
				}
				break;
			case 6:
				switch(correct) {
					case 3:
						amountWon = 1 * amountBet;
						break;
					case 4:
						amountWon = 7 * amountBet;
						break;
					case 5:
						amountWon = 50 * amountBet;
						break;
					case 6:
						amountWon = 1600 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least three numbers to win.");
						break;
				}
				break;
			case 7:
				switch(correct) {
					case 3:
						amountWon = 1 * amountBet;
						break;
					case 4:
						amountWon = 3 * amountBet;
						break;
					case 5:
						amountWon = 20 * amountBet;
						break;
					case 6:
						amountWon = 100 * amountBet;
						break;
					case 7:
						amountWon = 5000 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least three numbers to win.");
						break;
				}
				break;
			case 8:
				switch(correct) {
					case 4:
						amountWon = 2 * amountBet;
						break;
					case 5:
						amountWon = 10 * amountBet;
						break;
					case 6:
						amountWon = 50 * amountBet;
						break;
					case 7:
						amountWon = 1000 * amountBet;
						break;
					case 8:
						amountWon = 15000 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least 4 numbers to win.");
						break;
				}
				break;
			case 9:
				switch(correct) {
					case 4:
						amountWon = 1 * amountBet;
						break;
					case 5:
						amountWon = 5 * amountBet;
						break;
					case 6:
						amountWon = 25 * amountBet;
						break;
					case 7:
						amountWon = 200 * amountBet;
						break;
					case 8:
						amountWon = 4000 * amountBet;
						break;
					case 9:
						amountWon = 40000 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you must match at least 4 numbers to win.");
						break;
				}
				break;
			case 10:
				switch(correct) {
					case 0:
						amountWon = 2 * amountBet;
						break;
					case 5:
						amountWon = 2 * amountBet;
						break;
					case 6:
						amountWon = 20 * amountBet;
						break;
					case 7:
						amountWon = 80 * amountBet;
						break;
					case 8:
						amountWon = 500 * amountBet;
						break;
					case 9:
						amountWon = 10000 * amountBet;
						break;
					case 10:
						amountWon = 100000 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you match either zero numbers, or five to ten numbers to win.");
						break;
				}
				break;
			case 11:
				switch(correct) {
					case 0:
						amountWon = 2 * amountBet;
						break;
					case 5:
						amountWon = 1 * amountBet;
						break;
					case 6:
						amountWon = 10 * amountBet;
						break;
					case 7:
						amountWon = 50 * amountBet;
						break;
					case 8:
						amountWon = 250 * amountBet;
						break;
					case 9:
						amountWon = 1500 * amountBet;
						break;
					case 10:
						amountWon = 15000 * amountBet;
						break;
					case 11:
						amountWon = 500000 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you match either zero numbers, or five to eleven numbers to win.");
						break;
				}
				break;
			case 12:
				switch(correct) {
					case 0:
						amountWon = 4 * amountBet;
						break;
					case 6:
						amountWon = 5 * amountBet;
						break;
					case 7:
						amountWon = 25 * amountBet;
						break;
					case 8:
						amountWon = 150 * amountBet;
						break;
					case 9:
						amountWon = 1000 * amountBet;
						break;
					case 10:
						amountWon = 2500 * amountBet;
						break;
					case 11:
						amountWon = 25000 * amountBet;
						break;
					case 12:
						amountWon = 1000000 * amountBet;
						break;
					default:
						JOptionPane.showMessageDialog(null, "You matched " + correct + " numbers.  Sorry but you match either zero numbers, or six to twelve numbers to win.");
						break;
				}
				break;
		}

		if(amountWon > 0)
			JOptionPane.showMessageDialog(null, "Congratulations you matched " + correct + " number(s) and won " + currency.format(amountWon));
	}
	
	public void populateArrayList() {
		for (i = 0; i <= 79; i++) {
			shuffleNumbers.add(i);
		}
	}
	
	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// Handle ButtonListener from spots
			for (int i = 0; i < spots.length; i++) {
				if (spots[i].isSelected())
					numbersBet = Integer.parseInt(spots[i].getText());
			}
			
			if (count < numbersBet) {
				count++;
				
				if (e.getSource() instanceof JButton) {
					((JButton) e.getSource()).setIcon(icon);
					
					String numberLabel = ((JButton) e.getSource()).getText();
					picks.add(numberLabel);
					((JButton) e.getSource()).setText("");
				}
			} else if (count == 0) {
				JOptionPane.showMessageDialog(null, "You have to choose to bet at least one number");
				
				numbersBet = 0;
			} else {
				JOptionPane.showMessageDialog(null, "You have already chosen your " + numbersBet + " numbers.\nYou cannot choose any additional numbers");
			}
		}
	}
	
	private class Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// Listener to determine button pressed
			
			if (e.getSource() == betMaxButton)
				dollars[5].setSelected(true);
			
			if (e.getSource() == eraseButton)
				clear();
			
			if (e.getSource() == info)
				JOptionPane.showMessageDialog(null, "Click the numbers you wish to bet (1-12).\nClick Generate Numbers\n20 Random numbers will be chosen and highlighted in Red.\nA message will display as to whether you have won.\nSee Help > Payoffs for a list of payoffs.");

			if (e.getSource() == exit)
				System.exit(0);
			
			if (e.getSource() == startButton)
				generateNumbers();
			
			// Get Payoff list item selected
			int payoffSelected = -1;
			String payoffMsg = "";
			
			for (int i = 0; i < payoffs.length; i++) {
				if (e.getSource() == payoffs[i])
					payoffSelected = i;
			}
			
			if (payoffSelected > -1) {
				switch (payoffSelected) {
					case 0:
						payoffMsg += "Match\n1: $2.50";
						break;
					case 1:
						payoffMsg += "Match\n1: $1.00\n2: $5.00";
						break;
					case 2:
						payoffMsg += "Match\n2: $2.50\n3: $25.00";
						break;
					case 3:
						payoffMsg += "Match\n2: $1.00\n3: $4.00\n4: $100.00";
						break;
					case 4:
						payoffMsg += "Match\n3: $2.00\n4: $20.00\n5: $450.00";
						break;
					case 5:
						payoffMsg += "Match\n3: $1.00\n4: $7.00\n5: $50.00\n6: $1,600.00";
						break;
					case 6:
						payoffMsg += "Match\n3: $1.00\n4: $3.00\n5: $20.00\n6: $100.00\n7: $5,000.00";
						break;
					case 7:
						payoffMsg += "Match\n4: $2.00\n5: $10.00\n6: $50.00\n7: $1,000.00\n8: $15,000.00";
						break;
					case 8:
						payoffMsg += "Match\n4: $1.00\n5: $5.00\n6: $25.00\n7: $200.00\n8: $4,000.00\n9: $40,000.00";
						break;
					case 9:
						payoffMsg += "Match\n0: $2.00\n5: $2.00\n6: $20.00\n7: $80.00\n8: $500.00\n9: $10,000.00\n10: $100,000.00";
						break;
					case 10:
						payoffMsg += "Match\n0: $2.00\n5: $1.00\n6: $10.00\n7: $50.00\n8: $250.00\n9: $1,500.00\n10: $15,000.00\n11: $500,000.00";
						break;
					case 11:
						payoffMsg += "Match\n0: $4.00\n6: $5.00\n7: $25.00\n8: $150.00\n9: $1,000.00\n10: $2,500.00\n11: $25,000.00\n12: $1,000,000.00";
						break;
				}
				
				JOptionPane.showMessageDialog(null, payoffMsg);
				
				payoffSelected = -1;
			}
		}
	}
	
	private class RadioListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			// Handle RadioListener from spots
			JRadioButton rbChanged = (JRadioButton) arg0.getSource();
			
			for (int i = 0; i < spots.length; i++) {
				if (rbChanged == spots[i]) {
					numbersBet = Integer.parseInt(spots[i].getText());
					
					break;
				}
			}
			
			for (int i = 0; i < dollars.length; i++) {
				if (dollars[i].isSelected())
					amountBet = Integer.parseInt(dollars[i].getText());
			}
		}
	}
}
